/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARAMETER_STATE_H
#define PARAMETER_STATE_H

#include "normalize_value.h"

#include <base/source/fstreamer.h>
#include <pluginterfaces/vst/vsttypes.h>

#include <cstdint>
#include <functional>
#include <map>

namespace filurvst {

using convert_func = std::function<double(double value, double min, double max)>;

typedef struct {
  double min;
  double max;
  convert_func from_normalized;
  convert_func to_normalized;
  bool runtime;
} parameter_info_t;

class State {
 public:
  using parameter_map = std::map<Steinberg::Vst::ParamID, double>;
  using parameter_info_map = std::map<Steinberg::Vst::ParamID, parameter_info_t>;

  State() {
  }
  virtual ~State() {
  }

  virtual Steinberg::tresult setState(Steinberg::IBStream* stream);
  virtual Steinberg::tresult getState(Steinberg::IBStream* stream);

  void set(Steinberg::Vst::ParamID parameter_id, Steinberg::Vst::ParamValue value);
  Steinberg::Vst::ParamValue get(Steinberg::Vst::ParamID parameter_id);
  void setNormalized(Steinberg::Vst::ParamID parameter_id, Steinberg::Vst::ParamValue value);
  Steinberg::Vst::ParamValue getNormalized(Steinberg::Vst::ParamID parameter_id);
  bool info(Steinberg::Vst::ParamID parameter_id, parameter_info_t& info);
  double min(Steinberg::Vst::ParamID parameter_id);
  double max(Steinberg::Vst::ParamID parameter_id);

  void add(Steinberg::Vst::ParamID parameter_id,
           double default_value,
           double min = 0.0,
           double max = 1.0,
           convert_func from_normalized = normalized_to_value,
           convert_func to_normalized = value_to_normalized);
  void addSqr(Steinberg::Vst::ParamID parameter_id,
              double default_value,
              double min = 0.0,
              double max = 1.0);
  void addInt(Steinberg::Vst::ParamID parameter_id,
              double default_value,
              double min = 0.0,
              double max = 1.0);
  void addUInt(Steinberg::Vst::ParamID parameter_id,
               double default_value,
               double min = 0.0,
               double max = 1.0);
  void runtime(Steinberg::Vst::ParamID parameter_id, bool runtime);

  void update(Steinberg::Vst::ParamID parameter_id,
              double min = 0.0,
              double max = 1.0,
              convert_func from_normalized = normalized_to_value,
              convert_func to_normalized = value_to_normalized) {
    if (m_parameters.count(parameter_id) > 0) {
      double value = m_parameters.at(parameter_id);
      add(parameter_id, value, min, max, from_normalized, to_normalized);
    }
  }

  void updateSqr(Steinberg::Vst::ParamID parameter_id, double min = 0.0, double max = 1.0) {
    update(parameter_id, min, max, normalized_to_value_sqr, value_to_normalized_sqr);
  }

  void updateInt(Steinberg::Vst::ParamID parameter_id, double min = 0.0, double max = 1.0) {
    update(parameter_id, min, max, normalized_to_value_int, value_to_normalized_int);
  }

  void updateUInt(Steinberg::Vst::ParamID parameter_id, double min = 0.0, double max = 1.0) {
    update(parameter_id, min, max, normalized_to_value_int, value_to_normalized_int);
  }

  void remove(Steinberg::Vst::ParamID parameter_id);

  const parameter_map& parameters() const {
    return m_parameters;
  }

 protected:
  parameter_map m_parameters;
  parameter_info_map m_parameter_info;
};

}  // namespace filurvst

#endif // PARAMETER_STATE_H
