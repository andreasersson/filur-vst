/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NORMALIZE_VALUE_H
#define NORMALIZE_VALUE_H

#include <cstdint>

namespace filurvst {

double normalized_to_value(double value, double min, double max);
double value_to_normalized(double value, double min, double max);
double normalized_to_value_sqr(double value, double min, double max);
double value_to_normalized_sqr(double value, double min, double max);
double normalized_to_value_int(double value, double min, double max);
double value_to_normalized_int(double value, double min, double max);
double normalized_to_value_uint(double value, double min, double max);
double value_to_normalized_uint(double value, double min, double max);

} // namespace filurvst

#endif // NORMALIZE_VALUE_H
