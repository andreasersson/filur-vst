/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARAMETER_H
#define PARAMETER_H

#include "parameter_state.h"

#include <public.sdk/source/vst/vstparameters.h>

#include <functional>
#include <string>
#include <vector>

namespace filurvst {

using to_string_func = std::function<void(Steinberg::Vst::ParamValue value, Steinberg::Vst::String128 string)>;
using from_string_func = std::function<bool(const Steinberg::Vst::TChar* string, Steinberg::Vst::ParamValue& value)>;

class RangeParameter : public Steinberg::Vst::RangeParameter {
 public:
  RangeParameter(const Steinberg::Vst::TChar* title,
                 Steinberg::Vst::ParamID tag,
                 const Steinberg::Vst::TChar* units = nullptr,
                 Steinberg::Vst::ParamValue min = 0.0,
                 Steinberg::Vst::ParamValue max = 1.0,
                 Steinberg::Vst::ParamValue default_value = 0.0,
                 Steinberg::int32 step_count = 0, Steinberg::int32 flags = Steinberg::Vst::ParameterInfo::kCanAutomate,
                 Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId)
      : Steinberg::Vst::RangeParameter(title,
                                       tag,
                                       units,
                                       min,
                                       max,
                                       default_value,
                                       step_count, flags,
                                       unit_id) {
  }
  virtual ~RangeParameter() {
  }

  virtual void toString(Steinberg::Vst::ParamValue value,
                        Steinberg::Vst::String128 string) const override {
    if (m_to_string) {
      m_to_string(toPlain(value), string);
    } else {
      Steinberg::Vst::RangeParameter::toString(value, string);
    }
  }

  virtual bool fromString(const Steinberg::Vst::TChar* string,
                          Steinberg::Vst::ParamValue& value) const override {
    bool ret_val = false;

    if (m_from_string) {
      ret_val = m_from_string(string, value);
      value = toNormalized(value);
    } else {
      ret_val = Steinberg::Vst::RangeParameter::fromString(string, value);
    }

    return ret_val;
  }

  void setToString(to_string_func to_string) {
    m_to_string = to_string;
  }
  void setFromString(from_string_func from_string) {
    m_from_string = from_string;
  }

 private:
  to_string_func m_to_string;
  from_string_func m_from_string;
};

Steinberg::Vst::Parameter* add_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                         State &state,
                                         const std::string& name,
                                         Steinberg::int32 parameter_id,
                                         Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId);

Steinberg::Vst::Parameter* add_range_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                               State &state,
                                               const std::string& name,
                                               Steinberg::int32 parameter_id,
                                               Steinberg::Vst::ParamValue min,
                                               Steinberg::Vst::ParamValue max,
                                               Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId,
                                               int step_count = 0,
                                               to_string_func to_string = to_string_func(),
                                               from_string_func from_string = from_string_func());
Steinberg::Vst::Parameter* add_range_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                               State &state,
                                               const std::string& name,
                                               Steinberg::int32 parameter_id,
                                               Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId);
Steinberg::Vst::Parameter* add_range_parameter_step(Steinberg::Vst::ParameterContainer &parameters,
                                                    State &state,
                                                    const std::string& name,
                                                    Steinberg::int32 parameter_id,
                                                    Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId);

Steinberg::Vst::Parameter* add_on_off_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                State &state,
                                                const std::string& name,
                                                Steinberg::int32 parameter_id,
                                                Steinberg::Vst::UnitID unit_id = Steinberg::Vst::kRootUnitId);

Steinberg::Vst::Parameter* add_list_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                              State &state,
                                              const std::string& name,
                                              Steinberg::int32 parameter_id,
                                              const std::vector<std::string> &list,
                                              Steinberg::Vst::UnitID unit_id);

}  // namespace filurvst

#endif // PARAMETER_H
