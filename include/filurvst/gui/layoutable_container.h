/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTABLE_CONTAINER_H
#define LAYOUTABLE_CONTAINER_H

#include <vstgui/lib/cviewcontainer.h>

#include <memory>

namespace filurvst {
namespace gui {

class Layout;

class LayoutableContainer : public VSTGUI::CViewContainer {
 public:
  explicit LayoutableContainer(std::unique_ptr<Layout> layout,
                               const VSTGUI::CRect& rect = VSTGUI::CRect(0, 0, 0, 0),
                               const VSTGUI::CRect& margins = VSTGUI::CRect(0, 0, 0, 0));
  virtual ~LayoutableContainer() {}

  virtual bool addView(CView* view, CView* before = nullptr) override;
  virtual void setViewSize(const VSTGUI::CRect& rect, bool invalid = true) override;

  void layout();
  void setLayout(std::unique_ptr<Layout> layout) {
    m_layout = std::move(layout);
    this->layout();
  }
  Layout* getLayout() const { return m_layout.get(); }
  void setMargins(const VSTGUI::CRect &margins) {
    m_margins = margins;
    layout();
  }

  const VSTGUI::CRect& getMargins() const { return m_margins; }

 protected:
  std::unique_ptr<Layout> m_layout;
  VSTGUI::CRect m_margins;
};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUTABLE_CONTAINER_H
