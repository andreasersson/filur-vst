/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUT_H
#define LAYOUT_H

#include "layoutable_container.h"

namespace filurvst {
namespace gui {

class Layout {
 public:
  enum {
    kLayoutAttributeId = 12000
  };

  virtual ~Layout() {}

  virtual void layout(LayoutableContainer& layoutable) = 0;
};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUT_H
