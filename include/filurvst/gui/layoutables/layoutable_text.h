/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTABLE_TEXT_H
#define LAYOUTABLE_TEXT_H

#include <filurvst/gui/drawable.h>
#include <filurvst/gui/drawable_model.h>
#include <filurvst/gui/layoutable.h>

#include <vstgui/lib/controls/ctextlabel.h>
#include <vstgui/lib/controls/icontrollistener.h>

#include <memory>

namespace filurvst {
namespace gui {
class LayoutableText : public VSTGUI::CTextLabel {
 public:
  LayoutableText(VSTGUI::IControlListener* listener,
                 int32_t tag,
                 std::unique_ptr<Drawable> drawable);
  virtual ~LayoutableText() {}

  virtual void draw(VSTGUI::CDrawContext* context) override;
  virtual void setViewSize(const VSTGUI::CRect& rect, bool invalid = true) override;

  virtual void setDrawable(std::unique_ptr<Drawable> drawable) {
    m_drawable = std::move(drawable);
    setDirty();
  }
  Drawable* getDrawable() { return m_drawable.get(); }

 private:
  std::unique_ptr<Drawable> m_drawable;
  DrawableModel m_model;
};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUTABLE_TEXT_H
