/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTABLE_OPTION_MENU_H
#define LAYOUTABLE_OPTION_MENU_H

#include <filurvst/gui/drawable.h>
#include <filurvst/gui/layoutable.h>

#include <vstgui/lib/controls/coptionmenu.h>

#include <memory>

namespace filurvst {
namespace gui {

class LayoutableOptionMenu : public VSTGUI::COptionMenu {
 public:
  LayoutableOptionMenu(VSTGUI::IControlListener* listener,
                       int32_t tag,
                       std::unique_ptr<Drawable> drawable);
  virtual ~LayoutableOptionMenu() {}

  virtual void setViewSize(const VSTGUI::CRect& rect, bool invalid = true) override;
  virtual void draw(VSTGUI::CDrawContext* context) override;

  virtual void setDrawable(std::unique_ptr<Drawable> drawable) {
    m_drawable = std::move(drawable);
    setDirty();
  }
  virtual Drawable* getDrawable() { return m_drawable.get(); }

 protected:
  std::unique_ptr<Drawable> m_drawable;
  DrawableModel m_model;
};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUTABLE_OPTION_MENU_H
