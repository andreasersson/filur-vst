/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTABLE_VIEW_SWITCH_BUTTON_H
#define LAYOUTABLE_VIEW_SWITCH_BUTTON_H

#include <filurvst/gui/drawable.h>
#include <filurvst/gui/layoutable.h>

#include <vstgui/lib/controls/ccontrol.h>

#include <memory>

namespace filurvst {
namespace gui {

class LayoutableViewSwitchButton : public Layoutable {
 public:
  LayoutableViewSwitchButton(VSTGUI::IControlListener* listener,
                             int32_t tag,
                             float button_value,
                             float min_value,
                             float max_value,
                             std::unique_ptr<Drawable> drawable);
  virtual ~LayoutableViewSwitchButton() {}

  VSTGUI::CMouseEventResult onMouseDown(VSTGUI::CPoint& where,
                                        const VSTGUI::CButtonState& buttons)
                                            override;
  VSTGUI::CMouseEventResult onMouseUp(VSTGUI::CPoint& where,
                                      const VSTGUI::CButtonState& buttons)
                                          override;

  virtual void setButtonValue(float value) { m_button_value = value; }

 protected:
  virtual void updateModel(DrawableModel& model) override;

  float m_button_value;

};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUTABLE_VIEW_SWITCH_BUTTON_H
