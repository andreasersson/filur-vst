/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTABLE_KNOB_H
#define LAYOUTABLE_KNOB_H

#include <filurvst/gui/drawable.h>
#include <filurvst/gui/layoutable.h>

#include <vstgui/lib/controls/ccontrol.h>
#include <vstgui/lib/cframe.h>

namespace filurvst {
namespace gui {

class LayoutableKnob : public Layoutable {
 public:
  LayoutableKnob(VSTGUI::IControlListener* listener,
                 int32_t tag,
                 std::unique_ptr<Drawable> drawable);
  virtual ~LayoutableKnob() {
  }
  ;

  virtual void setStartAngle(double val);
  virtual double getStartAngle() const { return m_startAngle; }

  virtual void setRangeAngle(double val);
  virtual double getRangeAngle() const { return m_rangeAngle; }
  virtual void forceLinearMode(bool linear_mode) { m_force_linear_mode = linear_mode;}

  virtual VSTGUI::CMouseEventResult onMouseDown(VSTGUI::CPoint& where,
                                                const VSTGUI::CButtonState& buttons) override;
  virtual VSTGUI::CMouseEventResult onMouseUp(VSTGUI::CPoint& where,
                                              const VSTGUI::CButtonState& buttons) override;
  virtual VSTGUI::CMouseEventResult onMouseMoved(VSTGUI::CPoint& where,
                                                 const VSTGUI::CButtonState& buttons) override;

  virtual bool onWheel(const VSTGUI::CPoint& where,
                       const VSTGUI::CMouseWheelAxis& axis,
                       const float& distance,
                       const VSTGUI::CButtonState& buttons) override;

  virtual int32_t onKeyDown(VstKeyCode& keyCode) override;

 protected:
  virtual double valueFromPoint(VSTGUI::CPoint& point,
                                double range_angle,
                                double start_angle) const;

  double m_startAngle;
  double m_rangeAngle;
  double m_zoomFactor;
  bool m_force_linear_mode;

 private:
  VSTGUI::CPoint m_mouse_down_point;
  VSTGUI::CPoint m_previous_point;
  double m_circular_mode_value;
  double m_mouse_down_value;
  double m_linear_mode_scale;
  VSTGUI::CButtonState m_previous_button;
  enum VSTGUI::CKnobMode m_knob_mode;

  static const double s_knob_range;
};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUTABLE_KNOB_H
