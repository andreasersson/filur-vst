/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUT_FACTORY_H
#define LAYOUT_FACTORY_H

#include <filurvst/gui/layout.h>

#include <vstgui/uidescription/uiattributes.h>
#include <vstgui/uidescription/uidescription.h>

#include <memory>

namespace filurvst {
namespace gui {

std::unique_ptr<Layout> layout_factory(const VSTGUI::UIAttributes& attributes,
                                       const VSTGUI::IUIDescription* description);
bool layout_apply(const VSTGUI::UIAttributes& attributes,
                  const VSTGUI::IUIDescription* description,
                  Layout* layout);

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUT_FACTORY_H
