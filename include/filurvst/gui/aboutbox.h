/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILURVST_ABOUT_BOX_H
#define FILURVST_ABOUT_BOX_H

#include <filurvst/gui/layoutable_container.h>
#include <filurvst/gui/layout.h>

#include <base/source/fobject.h>

namespace filurvst {
namespace gui {

class AboutBox : public LayoutableContainer, public Steinberg::FObject {
 public:
  enum {
    kMsgOpen = Steinberg::IDependent::kStdChangeMessageLast + 1,
    kMsgClose
  };

  explicit AboutBox(std::unique_ptr<Layout> layout,
                    const VSTGUI::CRect& rect = VSTGUI::CRect(0, 0, 0, 0),
                    const VSTGUI::CRect& margins = VSTGUI::CRect(0, 0, 0, 0));
  virtual ~AboutBox();

  virtual VSTGUI::CMouseEventResult onMouseDown(VSTGUI::CPoint& where, const VSTGUI::CButtonState& buttons) SMTG_OVERRIDE;
  virtual VSTGUI::CMouseEventResult onMouseMoved(VSTGUI::CPoint& where, const VSTGUI::CButtonState& buttons) SMTG_OVERRIDE;
  virtual VSTGUI::CMouseEventResult onMouseUp(VSTGUI::CPoint& where, const VSTGUI::CButtonState& buttons) SMTG_OVERRIDE;

  virtual void PLUGIN_API update(FUnknown* changedUnknown, Steinberg::int32 message) SMTG_OVERRIDE;

  void setController(Steinberg::FObject* controller);

 private:
  void ref();
  void unref();
  void open();
  void close();

  Steinberg::FObject* m_fobject {nullptr};
};

}  // namespace gui
}  // namespace filurvst

#endif // FILURVST_ABOUT_BOX_H
