/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEXT_RECT_DRAWABLE_H
#define TEXT_RECT_DRAWABLE_H

#include <filurvst/gui/drawable.h>

namespace filurvst {
#include <string>

namespace gui {

class TextRectDrawable : public Drawable {
 public:
  TextRectDrawable(const std::string& text,
                   const VSTGUI::CColor& font_color,
                   const VSTGUI::CColor& bg_color,
                   const VSTGUI::CColor& active_bg_color,
                   VSTGUI::CFontRef font,
                   VSTGUI::CCoord radius,
                   VSTGUI::CCoord aspect_ratio);
  virtual ~TextRectDrawable() {
  }

  virtual void draw(VSTGUI::CDrawContext* context,
                    const VSTGUI::CRect& rect,
                    const DrawableModel& model) override;

  VSTGUI::CFontRef font = { nullptr };
  VSTGUI::CColor font_color;
  VSTGUI::CColor bg_color;
  VSTGUI::CColor active_bg_color;
  VSTGUI::CCoord radius = { 0.0 };
  VSTGUI::CCoord aspect_ratio = { 1.0 };
  std::string text;
};

}  // namespace gui
}  // namespace filurvst

#endif // TEXT_RECT_DRAWABLE_H
