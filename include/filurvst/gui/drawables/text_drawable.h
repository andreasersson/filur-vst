/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEXT_DRAWABLE_H
#define TEXT_DRAWABLE_H

#include <filurvst/gui/drawable.h>

namespace filurvst {
namespace gui {

class TextDrawable : public Drawable {
 public:
  TextDrawable(const std::string& text, const VSTGUI::CColor& color, VSTGUI::CFontRef font);
  virtual ~TextDrawable() {
  }

  virtual void draw(VSTGUI::CDrawContext* context,
                    const VSTGUI::CRect& rect,
                    const DrawableModel& model) override;

  VSTGUI::CFontRef font;
  VSTGUI::CColor color;
  std::string text;
};

}  // namespace gui
}  // namespace filurvst

#endif // TEXT_DRAWABLE_H
