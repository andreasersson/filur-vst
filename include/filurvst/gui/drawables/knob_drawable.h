/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef KNOB_DRAWABLE_H
#define KNOB_DRAWABLE_H

#include <filurvst/gui/drawable.h>

namespace filurvst {
namespace gui {

class KnobDrawable : public Drawable {
 public:
  KnobDrawable(const VSTGUI::CColor& color,
               const VSTGUI::CColor& bg_color,
               bool symmetric = false);
  virtual ~KnobDrawable() {
  }

  virtual void draw(VSTGUI::CDrawContext* context,
                    const VSTGUI::CRect& rect,
                    const DrawableModel& model) override;
  virtual void setSymmetric(bool symmetric);

  VSTGUI::CColor color;
  VSTGUI::CColor bg_color;

 private:
  bool m_symmetric;
  VSTGUI::CRect m_rect;
  VSTGUI::CCoord m_line_width;
  VSTGUI::CCoord m_start_angle;
  VSTGUI::CCoord m_range_angle;
  VSTGUI::CCoord m_center_angle;
};

}  // namespace gui
}  // namespace filurvst

#endif // KNOB_DRAWABLE_H
