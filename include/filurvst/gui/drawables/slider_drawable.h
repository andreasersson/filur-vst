/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SLIDER_DRAWABLE_H
#define SLIDER_DRAWABLE_H

#include <filurvst/gui/drawable.h>

namespace filurvst {
namespace gui {

class VerticalSliderDrawable : public Drawable {
 public:
  VerticalSliderDrawable(const VSTGUI::CColor& color, const VSTGUI::CColor& bg_color);
  virtual ~VerticalSliderDrawable() {
  }

  virtual void draw(VSTGUI::CDrawContext* context,
                    const VSTGUI::CRect& rect,
                    const DrawableModel& model) override;

  VSTGUI::CColor color;
  VSTGUI::CColor bg_color;
};

}  // namespace gui
}  // namespace filurvst

#endif // SLIDER_DRAWABLE_H
