/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ON_OFF_BUTTON_DRAWABLE_H
#define ON_OFF_BUTTON_DRAWABLE_H

#include <filurvst/gui/drawable.h>

namespace filurvst {
namespace gui {

class OnOffButtonDrawable : public Drawable {
 public:
  OnOffButtonDrawable(const std::string& on_text,
                      const std::string& off_text,
                      const VSTGUI::CColor& on_color,
                      const VSTGUI::CColor& off_color,
                      const VSTGUI::CColor& bg_color,
                      VSTGUI::CFontRef font,
                      VSTGUI::CCoord radius,
                      VSTGUI::CCoord aspect_ratio);
  virtual ~OnOffButtonDrawable() {
  }

  virtual void draw(VSTGUI::CDrawContext* context,
                    const VSTGUI::CRect& rect,
                    const DrawableModel& model) override;

  std::string on_text;
  std::string off_text;
  VSTGUI::CColor on_color;
  VSTGUI::CColor off_color;
  VSTGUI::CColor bg_color;
  VSTGUI::CCoord radius = { 0.0 };
  VSTGUI::CCoord aspect_ratio = { 1.0 };
  VSTGUI::CFontRef font;
};

}  // namespace gui
}  // namespace filurvst

#endif // ON_OFF_BUTTON_DRAWABLE_H
