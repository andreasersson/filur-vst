/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILL_LAYOUT_H
#define FILL_LAYOUT_H

#include <filurvst/gui/layout.h>
#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

class FillLayout : public Layout {
 public:
  virtual void layout(LayoutableContainer& layoutable) override;

  static void setIgnoreMargins(VSTGUI::CView* view, bool value);
};

}  // namespace gui
}  // namespace filurvst

#endif // FILL_LAYOUT_H
