/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GRID_LAYOUT_H
#define GRID_LAYOUT_H

#include <filurvst/gui/layout.h>
#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

class GridLayout : public Layout {
 public:
  GridLayout(int _number_of_columns,
             int _number_of_rows,
             double _horizontal_spacing = 0.125,
             double _vertical_spacing = 0.125)
      : number_of_columns(_number_of_columns),
        number_of_rows(_number_of_rows),
        h_spacing(_horizontal_spacing),
        v_spacing(_vertical_spacing) {
  }

  virtual void layout(LayoutableContainer& layoutable) override;

  int number_of_columns;
  int number_of_rows;
  double h_spacing;
  double v_spacing;
};

}  // namespace gui
}  // namespace filurvst

#endif // GRID_LAYOUT_H
