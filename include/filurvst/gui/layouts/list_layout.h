/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LIST_LAYOUT_H
#define LIST_LAYOUT_H

#include <filurvst/gui/layout.h>
#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

class ListLayout : public Layout {
 public:
  enum Orientation {
    kHorizontal,
    kVertical
  };

  ListLayout(enum Orientation _orientation = kHorizontal,
             double _spacing = 0.125)
      : orientation(_orientation),
        spacing(_spacing) {
  }
  virtual ~ListLayout() {
  }

  virtual void layout(LayoutableContainer& layoutable) override;

  enum Orientation orientation;
  double spacing;
};

}  // namespace gui
}  // namespace filurvst

#endif // LIST_LAYOUT_H
