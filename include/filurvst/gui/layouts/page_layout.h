/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PAGE_LAYOUT_H
#define PAGE_LAYOUT_H

#include <filurvst/gui/layout.h>
#include <filurvst/gui/layoutable_container.h>
#include <filurvst/gui/layoutable.h>

#include <vstgui/lib/cview.h>

namespace filurvst {
namespace gui {

class PageLayout : public Layout {
 public:
  enum LayoutId {
    kHeaderId = 0,
    kContentId
  };
  explicit PageLayout(double _header_height, double _spacing = 0.0)
  : header_height(_header_height),
    spacing(_spacing) {}
  virtual ~PageLayout() {}

  virtual void layout(LayoutableContainer& layoutable) override;

  static void setHeaderId(VSTGUI::CView* view);
  static void setContentId(VSTGUI::CView* view);

  double header_height;
  double spacing;
};

}  // namespace gui
}  // namespace filurvst

#endif // PAGE_LAYOUT_H
