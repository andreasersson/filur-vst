/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARAMETER_LAYOUT_H
#define PARAMETER_LAYOUT_H

#include <filurvst/gui/layout.h>
#include <filurvst/gui/layoutable_container.h>

#include <vstgui/lib/cview.h>
#include <vstgui/lib/cfont.h>

namespace filurvst {
namespace gui {

class ParameterLayout : public Layout {
 public:
  enum LayoutId {
    kLabelId = 0,
    kControlId
  };

  explicit ParameterLayout(VSTGUI::CFontRef _label_font, double _control_scale = 1.0, double _spacing = 0)
  : label_font(_label_font),
    control_scale(_control_scale),
    spacing(_spacing) {
    control_scale = std::max(std::min(control_scale, 1.0), 0.0);
  }
  virtual ~ParameterLayout() {}

  virtual void layout(LayoutableContainer& layoutable) override;

  static void setLabelId(VSTGUI::CView* view);
  static void setControlId(VSTGUI::CView* view);

  VSTGUI::CFontRef label_font;
  double control_scale;
  double spacing;
};

}  // namespace gui
}  // namespace filurvst

#endif // PARAMETER_LAYOUT_H
