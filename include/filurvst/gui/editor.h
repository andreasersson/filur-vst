/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILURVST_GUI_EDITOR_H
#define FILURVST_GUI_EDITOR_H

#include <vstgui/plugin-bindings/vst3editor.h>

#include <vector>

namespace filurvst {
namespace gui {

class FontResizeEditor : public VSTGUI::VST3Editor {
 public:
  FontResizeEditor(Steinberg::Vst::EditController* controller,
         VSTGUI::UTF8StringPtr template_name,
         VSTGUI::UTF8StringPtr xml_file);
  Steinberg::tresult PLUGIN_API onSize(Steinberg::ViewRect* new_size) override;
  virtual ~FontResizeEditor() {}

 private:
  struct Font {
    explicit Font(VSTGUI::CFontRef _font) :
      font(_font),
      size(_font->getSize()) {}
    VSTGUI::CFontRef font;
    VSTGUI::CCoord size;
  };
  void resizeFonts(VSTGUI::CCoord size);

  std::vector<Font> m_fonts;
  VSTGUI::CPoint m_original_size;
  VSTGUI::CCoord m_font_scale = {1.0};
};

}  // namespace gui
}  // namespace filurvst

#endif // FILURVST_GUI_EDITOR_H
