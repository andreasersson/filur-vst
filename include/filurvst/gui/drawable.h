/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "drawable_model.h"

#include <vstgui/lib/cdrawcontext.h>
#include <vstgui/lib/crect.h>

namespace filurvst {
namespace gui {

class Drawable {
 public:
  Drawable() {}
  virtual ~Drawable() {}

  virtual void draw(VSTGUI::CDrawContext* context,
                    const VSTGUI::CRect& rect,
                    const DrawableModel& model) = 0;
};

}  // namespace gui
}  // namespace filurvst

#endif // DRAWABLE_H
