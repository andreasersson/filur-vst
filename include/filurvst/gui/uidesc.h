/*
 * Copyright 2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FILUR_UIDESC_H
#define FILUR_UIDESC_H

#include <vstgui/lib/ccolor.h>
#include <vstgui/lib/cfont.h>
#include <vstgui/uidescription/uiattributes.h>
#include <vstgui/uidescription/uidescription.h>

#include <string>

namespace filurvst {
namespace gui {

bool get_font(const VSTGUI::IUIDescription* description,
              const std::string* font_name,
              VSTGUI::CFontRef& font);
bool get_string_attribute(const VSTGUI::UIAttributes& attributes,
                          const std::string& name,
                          std::string& value);
bool string_to_color(const VSTGUI::IUIDescription* description,
                     const std::string* value,
                     VSTGUI::CColor& color);

}  // namespace gui
}  // namespace filurvst

#endif // FILUR_UIDESC_H
