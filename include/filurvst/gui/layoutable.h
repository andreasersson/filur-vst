/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LAYOUTABLE_H
#define LAYOUTABLE_H

#include "drawable.h"
#include "drawable_model.h"

#include <vstgui/lib/controls/ccontrol.h>

#include <memory>

namespace filurvst {
namespace gui {

class Layoutable : public VSTGUI::CControl {
 public:
  Layoutable(VSTGUI::IControlListener* listener,
             int32_t tag,
             std::unique_ptr<Drawable> drawable);
  explicit Layoutable(std::unique_ptr<Drawable> drawable);
  Layoutable(const Layoutable& layoutable);
  virtual ~Layoutable() {};

  virtual void setViewSize(const VSTGUI::CRect& rect, bool invalid = true) override;
  virtual void draw(VSTGUI::CDrawContext* context) override;

  virtual void setDrawable(std::unique_ptr<Drawable> drawable) {
    m_drawable = std::move(drawable);
    setDirty();
  }
  Drawable* getDrawable() { return m_drawable.get(); }

  CLASS_METHODS(Layoutable, VSTGUI::CControl)

 protected:
  virtual void resize(const VSTGUI::CRect& rect) {
    (void) rect;
  }
  virtual void updateModel(DrawableModel& model) {
    model.value = getValueNormalized();
  }

  std::unique_ptr<Drawable> m_drawable;
  DrawableModel m_model;
};

}  // namespace gui
}  // namespace filurvst

#endif // LAYOUTABLE_H
