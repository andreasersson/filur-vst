/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <filurvst/common/tlv.h>

#include <public.sdk/source/common/memorystream.h>

TEST(tlv, read_fail_empty_stream) {
  Steinberg::MemoryStream stream;
  Steinberg::IBStreamer streamer(&stream);
  Steinberg::uint32 tag = 0;
  Steinberg::uint32 length = 0;

  EXPECT_FALSE(filurvst::tlv::read(streamer, tag, length));
}

TEST(tlv, write_read) {
  Steinberg::MemoryStream stream;
  Steinberg::IBStreamer streamer(&stream);
  enum {
    tag_1,
    tag_2,
    tag_3,
  };
  double expected_value_1 = 17.0;
  Steinberg::int32 expected_value_2 = 18;
  Steinberg::uint32 expected_value_3 = 19;
  Steinberg::uint32 tag;
  Steinberg::uint32 length;
  double value_1 = 0.0;
  Steinberg::int32 value_2 = 0;
  Steinberg::uint32 value_3 = 0;

  EXPECT_TRUE(filurvst::tlv::write(streamer, tag_1, expected_value_1));
  EXPECT_TRUE(filurvst::tlv::write(streamer, tag_2, expected_value_2));
  EXPECT_TRUE(filurvst::tlv::write(streamer, tag_3, expected_value_3));

  streamer.seek(0, Steinberg::kSeekSet);

  EXPECT_TRUE(filurvst::tlv::read(streamer, tag, length));
  EXPECT_EQ(tag_1, tag);
  EXPECT_EQ(sizeof(expected_value_1), length);
  EXPECT_TRUE(filurvst::tlv::read_value(streamer, value_1));
  EXPECT_EQ(expected_value_1, value_1);

  EXPECT_TRUE(filurvst::tlv::read(streamer, tag, length));
  EXPECT_EQ(tag_2, tag);
  EXPECT_EQ(sizeof(expected_value_2), length);
  EXPECT_TRUE(filurvst::tlv::read_value(streamer, value_2));
  EXPECT_EQ(expected_value_2, value_2);

  EXPECT_TRUE(filurvst::tlv::read(streamer, tag, length));
  EXPECT_EQ(tag_3, tag);
  EXPECT_EQ(sizeof(expected_value_3), length);
  EXPECT_TRUE(filurvst::tlv::read_value(streamer, value_3));
  EXPECT_EQ(expected_value_3, value_3);
}

TEST(tlv, skip) {
  Steinberg::MemoryStream stream;
  Steinberg::IBStreamer streamer(&stream);
  enum {
    tag_1,
    tag_2,
    tag_3,
  };
  double expected_value_1 = 17.0;
  Steinberg::int32 expected_value_2 = 18;
  Steinberg::uint32 expected_value_3 = 19;
  Steinberg::uint32 tag;
  Steinberg::uint32 length;
  double value_1 = 0.0;
  Steinberg::uint32 value_3 = 0;
  EXPECT_TRUE(filurvst::tlv::write(streamer, tag_1, expected_value_1));
  EXPECT_TRUE(filurvst::tlv::write(streamer, tag_2, expected_value_2));
  EXPECT_TRUE(filurvst::tlv::write(streamer, tag_3, expected_value_3));

  streamer.seek(0, Steinberg::kSeekSet);

  EXPECT_TRUE(filurvst::tlv::read(streamer, tag, length));
  EXPECT_EQ(tag_1, tag);
  EXPECT_EQ(sizeof(expected_value_1), length);
  EXPECT_TRUE(filurvst::tlv::read_value(streamer, value_1));
  EXPECT_EQ(expected_value_1, value_1);

  EXPECT_TRUE(filurvst::tlv::read(streamer, tag, length));
  EXPECT_EQ(tag_2, tag);
  EXPECT_EQ(sizeof(expected_value_2), length);
  EXPECT_TRUE(filurvst::tlv::skip(streamer, length));

  EXPECT_TRUE(filurvst::tlv::read(streamer, tag, length));
  EXPECT_EQ(tag_3, tag);
  EXPECT_EQ(sizeof(expected_value_3), length);
  EXPECT_TRUE(filurvst::tlv::read_value(streamer, value_3));
  EXPECT_EQ(expected_value_3, value_3);
}
