/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>

#include <filurvst/common/normalize_value.h>

#include <cmath>

TEST(normalized_value, double_to_from_normalized) {
  double min = -100.0;
  double max = 100.0;
  double step = 0.01;
  double acceptable_diff = 3e-14;
  for (double value = min; value <= max; value += step) {
    double normalized_value = filurvst::value_to_normalized(value, min, max);
    EXPECT_LE(normalized_value, 1.0);
    EXPECT_GE(normalized_value, 0.0);
    double local_value = filurvst::normalized_to_value(normalized_value, min, max);
    EXPECT_LE(local_value, max);
    EXPECT_GE(local_value, min);
    double diff = fabs(local_value - value);
    EXPECT_LE(diff, acceptable_diff);
  }
}

TEST(normalized_value, double_sqr_to_from_normalized) {
  double min = -100.0;
  double max = 100.0;
  double step = 0.01;
  double acceptable_diff = 5e-14;
  for (double value = min; value <= max; value += step) {
    double normalized_value = filurvst::value_to_normalized_sqr(value, min, max);
    EXPECT_LE(normalized_value, 1.0);
    EXPECT_GE(normalized_value, 0.0);
    double local_value = filurvst::normalized_to_value_sqr(normalized_value, min, max);
    EXPECT_LE(local_value, max);
    EXPECT_GE(local_value, min);
    double diff = fabs(local_value - value);
    EXPECT_LE(diff, acceptable_diff);
  }
}

TEST(normalized_value, int_to_from_normalized) {
  double min = -100.0;
  double max = 100.0;
  for (double value = min; value <= max; value += 1.0) {
    double normalized_value = filurvst::value_to_normalized_int(value, min, max);
    EXPECT_LE(normalized_value, 1.0);
    EXPECT_GE(normalized_value, 0.0);
    double local_value = std::round(filurvst::normalized_to_value(normalized_value, min, max));
    EXPECT_LE(local_value, max);
    EXPECT_GE(local_value, min);
    EXPECT_EQ(local_value, value);
  }
}

TEST(normalized_value, uint_to_from_normalized) {
  double min = 0.0;
  double max = 100.0;
  for (double value = min; value <= max; value += 1.0) {
    double normalized_value = filurvst::value_to_normalized_uint(value, min, max);
    EXPECT_LE(normalized_value, 1.0);
    EXPECT_GE(normalized_value, 0.0);
    double local_value = std::round(filurvst::normalized_to_value(normalized_value, min, max));
    EXPECT_LE(local_value, max);
    EXPECT_GE(local_value, min);
    EXPECT_EQ(local_value, value);
  }
}
