/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <pluginterfaces/base/funknown.h>

#include <iostream>

int main() {
  int ret_val = 0;

  Steinberg::FUID fuid;
  bool result = fuid.generate();

  if (result) {
    std::cout << "UID: " << std::hex << std::uppercase <<
        "0x" << fuid.getLong1() << " " <<
        "0x" << fuid.getLong2() << " " <<
        "0x" << fuid.getLong3() << " " <<
        "0x" << fuid.getLong4() << " " <<
        std::dec << std::endl;
  } else {
    std::cerr << "ERROR: Failed to generate UID" << std::endl;
    ret_val = -1;
  }

  return ret_val;
}
