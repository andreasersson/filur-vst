/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/slider_drawable.h>

namespace filurvst {
namespace gui {

VerticalSliderDrawable::VerticalSliderDrawable(const VSTGUI::CColor& _color,
                                               const VSTGUI::CColor& _bg_color)
    : color(_color),
      bg_color(_bg_color) {
}

void VerticalSliderDrawable::draw(VSTGUI::CDrawContext* context,
                                  const VSTGUI::CRect& rect,
                                  const DrawableModel& model) {
  VSTGUI::CRect local_rect = rect;
  local_rect.inset(0.5, 0.5);
  VSTGUI::CCoord width = local_rect.getWidth() * 0.25;
  VSTGUI::CCoord x = (local_rect.getWidth() - width) * 0.5;
  context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);
  {
    VSTGUI::CRect bg_rect(x, local_rect.top, x + width, local_rect.bottom);
    VSTGUI::CGraphicsPath *path = context->createRoundRectGraphicsPath(bg_rect, width * 0.5);

    if (nullptr != path) {
      context->setFillColor(bg_color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
    }
  }
  {
    VSTGUI::CRect slider_rect(x,
                              local_rect.bottom - width - (local_rect.getHeight() - width) * model.value,
                              x + width,
                              local_rect.bottom);
    VSTGUI::CGraphicsPath *path = context->createRoundRectGraphicsPath(slider_rect,
                                                                       width * 0.5);

    if (nullptr != path) {
      context->setFillColor(color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
