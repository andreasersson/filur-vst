/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/knob_drawable.h>

#include <vstgui/lib/cgraphicspath.h>

#include <cmath>

#ifndef M_PI_4
  #define M_PI_4 0.785398163397448309615660845819875721  /* pi/4 */
#endif

namespace filurvst {
namespace gui {

static void add_arc(VSTGUI::CGraphicsPath* path,
                    const VSTGUI::CRect& rect,
                    double start_angle, double sweep_angle);
static void draw_knob(VSTGUI::CDrawContext* context,
                      const VSTGUI::CRect& rect,
                      const DrawableModel& model,
                      const VSTGUI::CColor& color,
                      const VSTGUI::CColor& bg_color,
                      VSTGUI::CCoord start_angle,
                      VSTGUI::CCoord m_range_angle);
static void draw_symmetric_knob(VSTGUI::CDrawContext* context,
                                const VSTGUI::CRect& rect,
                                const DrawableModel& model,
                                const VSTGUI::CColor& color,
                                const VSTGUI::CColor& bg_color,
                                VSTGUI::CCoord start_angle,
                                VSTGUI::CCoord range_angle,
                                VSTGUI::CCoord center_angle);

KnobDrawable::KnobDrawable(const VSTGUI::CColor& _color,
                           const VSTGUI::CColor& _bg_color,
                           bool symmetric)
    : color(_color),
      bg_color(_bg_color),
      m_symmetric(symmetric),
      m_rect(0, 0, 0, 0),
      m_line_width(0),
      m_start_angle(3.0 * M_PI / 4.0),
      m_range_angle(3.0 * M_PI / 2.0),
      m_center_angle(0.0) {
  KnobDrawable::setSymmetric(m_symmetric);
}

void KnobDrawable::draw(VSTGUI::CDrawContext* context,
                        const VSTGUI::CRect& rect,
                        const DrawableModel& model) {
  if (m_rect != rect) {
    m_rect = rect;

    double scale_factor = 1.0 + 0.5 * (1.0 - sin(M_PI_4));
    VSTGUI::CCoord knob_size = std::min(m_rect.getWidth() / scale_factor,
                                        m_rect.getHeight());
    knob_size = knob_size * scale_factor;
    VSTGUI::CCoord ofs = 0;
    if (knob_size > m_rect.getHeight()) {
      ofs = (knob_size - m_rect.getHeight()) * 0.5;
    }

    m_rect(0, 0, knob_size, knob_size);
    m_rect.offset((rect.getWidth() - knob_size) * 0.5,
                  (rect.getHeight() - knob_size) * 0.5 + ofs);

    m_line_width = 0.25 * m_rect.getWidth();

    VSTGUI::CCoord inset = m_line_width * 0.5;
    m_rect.inset(inset, inset);
  }

  context->setLineStyle(VSTGUI::kLineSolid);
  context->setLineWidth(m_line_width);
  context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);

  VSTGUI::CRect local_rect = m_rect;
  local_rect.inset(0.5, 0.5);

  if (m_symmetric) {
    draw_symmetric_knob(context, local_rect, model, color, bg_color, m_start_angle, m_range_angle, m_center_angle);
  } else {
    draw_knob(context, local_rect, model, color, bg_color, m_start_angle, m_range_angle);
  }
}

void KnobDrawable::setSymmetric(bool symmetric) {
  m_symmetric = symmetric;

  if (m_symmetric) {
    m_start_angle = 3.0 * M_PI / 4.0;
    m_center_angle = 3.0 * M_PI / 2.0;
    m_range_angle = 3.0 * M_PI / 4.0;
  } else {
    m_start_angle = 3.0 * M_PI / 4.0;
    m_range_angle = 3.0 * M_PI / 2.0;
    m_center_angle = 0.0;
  }
}

static void add_arc(VSTGUI::CGraphicsPath* path,
                    const VSTGUI::CRect& rect,
                    double start_angle,
                    double sweep_angle) {
  VSTGUI::CCoord w = rect.getWidth();
  VSTGUI::CCoord h = rect.getHeight();
  double end_angle = start_angle + sweep_angle;
  if (w != h) {
    start_angle = atan2(sin(start_angle) * h, cos(start_angle) * w);
    end_angle = atan2(sin(end_angle) * h, cos(end_angle) * w);
  }
  path->addArc(rect, start_angle / M_PI * 180, end_angle / M_PI * 180, sweep_angle >= 0);
}

static void draw_knob(VSTGUI::CDrawContext* context,
                      const VSTGUI::CRect& rect,
                      const DrawableModel& model,
                      const VSTGUI::CColor& color,
                      const VSTGUI::CColor& bg_color,
                      VSTGUI::CCoord start_angle,
                      VSTGUI::CCoord range_angle) {
  {
    auto path = owned(context->createGraphicsPath());
    if (nullptr != path) {
      add_arc(path, rect, start_angle, range_angle);
      context->setFrameColor(bg_color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathStroked);
    }
  }

  {
    auto path = owned(context->createGraphicsPath());
    if (nullptr != path) {
      VSTGUI::CCoord value = std::max(std::min(model.value, 1.0), 0.0);
      VSTGUI::CCoord width_angle = M_PI * 0.08;
      add_arc(path, rect, start_angle, width_angle + (range_angle - width_angle) * value);
      context->setFrameColor(color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathStroked);
    }
  }
}

static void draw_symmetric_knob(VSTGUI::CDrawContext* context,
                                const VSTGUI::CRect& rect,
                                const DrawableModel& model,
                                const VSTGUI::CColor& color,
                                const VSTGUI::CColor& bg_color,
                                VSTGUI::CCoord start_angle,
                                VSTGUI::CCoord range_angle,
                                VSTGUI::CCoord center_angle) {
  VSTGUI::CCoord value = std::max(std::min(model.value, 1.0), 0.0);
  value = 2.0 * (value - 0.5);

  {
    auto path = owned(context->createGraphicsPath());
    if (nullptr != path) {
      add_arc(path, rect, start_angle, 2 * range_angle);
      context->setFrameColor(bg_color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathStroked);
    }
  }

  {
    auto path = owned(context->createGraphicsPath());
    if (nullptr != path) {
      add_arc(path, rect, center_angle, range_angle * value);
      context->setFrameColor(color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathStroked);
    }
  }

  {
    auto path = owned(context->createGraphicsPath());
    if (nullptr != path) {
      VSTGUI::CCoord width_angle = M_PI * 0.040;
      VSTGUI::CCoord local_start_angle;
      VSTGUI::CCoord local_range_angle;
      if (value >= 0) {
        local_range_angle = 2 * width_angle;
        local_start_angle = center_angle - width_angle + (range_angle - width_angle) * value;

      } else {
        local_range_angle = -2 * width_angle;
        local_start_angle = center_angle + width_angle + (range_angle - width_angle) * value;
      }

      add_arc(path, rect, local_start_angle, local_range_angle);
      context->setFrameColor(color);
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathStroked);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
