/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/on_off_button_drawable.h>

namespace filurvst {
namespace gui {

OnOffButtonDrawable::OnOffButtonDrawable(const std::string& _on_text,
                                         const std::string& _off_text,
                                         const VSTGUI::CColor& _on_color,
                                         const VSTGUI::CColor& _off_color,
                                         const VSTGUI::CColor& _bg_color,
                                         VSTGUI::CFontRef _font,
                                         VSTGUI::CCoord _radius,
                                         VSTGUI::CCoord _aspect_ratio)
    : on_text(_on_text),
      off_text(_off_text),
      on_color(_on_color),
      off_color(_off_color),
      bg_color(_bg_color),
      radius(_radius),
      aspect_ratio(_aspect_ratio),
      font(_font) {}

void OnOffButtonDrawable::draw(VSTGUI::CDrawContext *context,
                               const VSTGUI::CRect& rect,
                               const DrawableModel& model) {
  context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);

  context->setFillColor(bg_color);
  VSTGUI::CRect local_rect = rect;
  local_rect.inset(0.5, 0.5);
  {
    VSTGUI::CCoord height = std::min(local_rect.getWidth() * aspect_ratio, local_rect.getHeight());
    local_rect.setHeight(height);
    local_rect.centerInside(rect);
    VSTGUI::CCoord local_radius = std::min(local_rect.getHeight() * radius, local_rect.getWidth());
    VSTGUI::CGraphicsPath *path = context->createRoundRectGraphicsPath(local_rect, local_radius);
    if (nullptr != path) {
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
    }
  }

  context->setFont(font);

  if (model.value > 0.5f) {
    context->setFontColor(on_color);
    context->drawString(on_text.c_str(), local_rect);
  } else {
    context->setFontColor(off_color);
    context->drawString(off_text.c_str(), local_rect);
  }
}

}  // namespace gui
}  // namespace filurvst
