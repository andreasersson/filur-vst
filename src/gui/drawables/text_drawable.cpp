/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/text_drawable.h>

namespace filurvst {
namespace gui {

TextDrawable::TextDrawable(const std::string& _text,
                           const VSTGUI::CColor& _color,
                           VSTGUI::CFontRef _font)
    : font(_font),
      color(_color),
      text(_text) {
}

void TextDrawable::draw(VSTGUI::CDrawContext* context,
                        const VSTGUI::CRect& rect,
                        const DrawableModel& model) {
  const char* c_str = nullptr;
  if (text.empty()) {
    c_str = model.text.data();
  } else {
    c_str = text.data();
  }
  context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);
  context->setFont(font);
  context->setFontColor(color);
  VSTGUI::CRect local_rect = rect;
  local_rect.inset(0.5, 0.5);
  context->drawString(c_str, local_rect);
}

}  // namespace gui
}  // namespace filurvst
