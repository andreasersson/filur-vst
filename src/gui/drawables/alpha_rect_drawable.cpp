/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/alpha_rect_drawable.h>

namespace filurvst {
namespace gui {

AlphaRectDrawable::AlphaRectDrawable(const VSTGUI::CColor& _color,
                                     VSTGUI::CCoord _radius)
    : color(_color),
      bg_color(VSTGUI::kTransparentCColor),
      radius(_radius) {
}

AlphaRectDrawable::AlphaRectDrawable(const VSTGUI::CColor& _color,
                                     const VSTGUI::CColor& _bg_color,
                                     VSTGUI::CCoord _radius)
    : color(_color),
      bg_color(_bg_color),
      radius(_radius) {
}

void AlphaRectDrawable::draw(VSTGUI::CDrawContext* context,
                             const VSTGUI::CRect& rect,
                             const DrawableModel& model) {
  uint8_t alpha = 0xff * model.value;

  context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);
  VSTGUI::CRect local_rect = rect;
  local_rect.inset(0.5, 0.5);
  VSTGUI::CCoord local_radius = std::min(local_rect.getHeight() * radius, local_rect.getWidth());
  VSTGUI::CGraphicsPath *path = context->createRoundRectGraphicsPath(local_rect, local_radius);

  if ((nullptr != path) && (bg_color.alpha > 0)) {
    context->setFillColor(bg_color);
    context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
  }

  if ((nullptr != path) && (alpha > 0)) {
    VSTGUI::CColor alpha_color = color;
    alpha_color.alpha = alpha;
    context->setFillColor(alpha_color);
    context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
  }
}

}  // namespace gui
}  // namespace filurvst
