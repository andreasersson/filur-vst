/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/rect_drawable.h>

namespace filurvst {
namespace gui {

RectDrawable::RectDrawable(const VSTGUI::CColor& _color, VSTGUI::CCoord _radius)
    : color(_color),
      active_color(_color),
      radius(_radius) {
}

RectDrawable::RectDrawable(const VSTGUI::CColor& _color,
                           const VSTGUI::CColor& _active_color,
                           VSTGUI::CCoord _radius)
    : color(_color),
      active_color(_active_color),
      radius(_radius) {
}

void RectDrawable::draw(VSTGUI::CDrawContext* context,
                        const VSTGUI::CRect& rect,
                        const DrawableModel& model) {
  VSTGUI::CColor* local_color = &color;
  if (model.value >= 0.5) {
    local_color = &active_color;
  }

  VSTGUI::CRect local_rect = rect;
  local_rect.inset(0.5, 0.5);
  VSTGUI::CCoord local_radius = std::min(local_rect.getHeight() * radius, local_rect.getWidth());
  VSTGUI::CGraphicsPath *path = context->createRoundRectGraphicsPath(local_rect, local_radius);

  if (nullptr != path) {
    context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);
    context->setFillColor(*local_color);
    context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
  }
}

}  // namespace gui
}  // namespace filurvst
