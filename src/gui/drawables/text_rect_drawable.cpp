/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawables/text_rect_drawable.h>

namespace filurvst {
namespace gui {

TextRectDrawable::TextRectDrawable(const std::string& _text,
                                   const VSTGUI::CColor& _color,
                                   const VSTGUI::CColor& _bg_color,
                                   const VSTGUI::CColor& _active_bg_color,
                                   VSTGUI::CFontRef _font,
                                   VSTGUI::CCoord _radius,
                                   VSTGUI::CCoord _aspect_ratio)
: font(_font),
  font_color(_color),
  bg_color(_bg_color),
  active_bg_color(_active_bg_color),
  radius(_radius),
  aspect_ratio(_aspect_ratio),
  text(_text) {
}

void TextRectDrawable::draw(VSTGUI::CDrawContext* context,
                            const VSTGUI::CRect& rect,
                            const DrawableModel& model) {
  context->setDrawMode(VSTGUI::kAntiAliasing | VSTGUI::kNonIntegralMode);

  const char* c_str = nullptr;

  if (!text.empty()) {
    c_str = text.data();
  }
  if ((nullptr == c_str) && !model.text.empty()) {
    c_str = model.text.data();
  }

  if (model.value < 0.5) {
    context->setFillColor(bg_color);
  } else {
    context->setFillColor(active_bg_color);
  }

  {
    VSTGUI::CRect local_rect = rect;
    local_rect.inset(0.5, 0.5);
    VSTGUI::CCoord height = std::min(local_rect.getWidth() * aspect_ratio, local_rect.getHeight());
    local_rect.setHeight(height);
    local_rect.centerInside(rect);
    VSTGUI::CCoord local_radius = std::min(local_rect.getHeight() * radius, local_rect.getWidth());
    VSTGUI::CGraphicsPath *path = context->createRoundRectGraphicsPath(local_rect, local_radius);
    if (nullptr != path) {
      context->drawGraphicsPath(path, VSTGUI::CDrawContext::kPathFilled);
    }
  }

  if (nullptr != c_str) {
    context->setFont(font);
    context->setFontColor(font_color);
    context->drawString(c_str, rect);
  }
}

}  // namespace gui
}  // namespace filurvst
