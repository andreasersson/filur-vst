/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutable_container.h>

#include <filurvst/gui/layout.h>

namespace filurvst {
namespace gui {

LayoutableContainer::LayoutableContainer(std::unique_ptr<Layout> layout,
                                         const VSTGUI::CRect& rect,
                                         const VSTGUI::CRect& margins)
    : VSTGUI::CViewContainer(rect),
      m_layout(std::move(layout)),
      m_margins(margins) {
  setTransparency(true);
}

bool LayoutableContainer::addView(CView* view, CView* before) {
  bool ret_val = VSTGUI::CViewContainer::addView(view, before);
  if (ret_val) {
    layout();
  }

  return ret_val;
}

void LayoutableContainer::setViewSize(const VSTGUI::CRect& rect, bool invalid) {
  if (rect != getViewSize()) {
    CViewContainer::setViewSize(rect, invalid);
    setMouseableArea(rect);
    layout();
  }
}

void LayoutableContainer::layout() {
  if (m_layout) {
    m_layout->layout(*this);
  }
}

}  // namespace gui
}  // namespace filurvst
