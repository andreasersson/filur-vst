/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/editor.h>

namespace filurvst {
namespace gui {

FontResizeEditor::FontResizeEditor(Steinberg::Vst::EditController* controller,
                                 VSTGUI::UTF8StringPtr template_name,
                                 VSTGUI::UTF8StringPtr xml_file)
    : VST3Editor(controller, template_name, xml_file) {
  if (nullptr != description) {
    std::list<const std::string*> fonts;
    description->collectFontNames(fonts);

    for (auto& font_name : fonts) {
      if ((nullptr != font_name) && (std::string::npos == font_name->find('~'))) {
        VSTGUI::CFontRef font_ref = description->getFont(font_name->c_str());
        m_fonts.push_back(Font(font_ref));
      }
    }
  }
}

Steinberg::tresult FontResizeEditor::onSize(Steinberg::ViewRect* new_size) {
  VSTGUI::CPoint zero(0.0, 0.0);
  if ((nullptr != new_size) && (m_original_size == zero)) {
    m_original_size.x = new_size->getWidth();
    m_original_size.y = new_size->getHeight();
  }

  VSTGUI::CCoord scale = 1.0;

  if ((nullptr != new_size) && (m_original_size != zero)) {
    VSTGUI::CPoint new_point(new_size->getWidth(), new_size->getHeight());
    VSTGUI::CCoord width_scale = new_point.x / m_original_size.x;
    VSTGUI::CCoord height_scale = new_point.y / m_original_size.y;
    scale = std::min(width_scale, height_scale);
  }

  resizeFonts(scale);

  return VSTGUI::VST3Editor::onSize(new_size);
}

void FontResizeEditor::resizeFonts(VSTGUI::CCoord scale) {
  if (m_font_scale != scale) {
    m_font_scale = scale;
    for (auto& font : m_fonts) {
      font.font->setSize(font.size * m_font_scale);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
