/*
 * Copyright 2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/uidesc.h>

#include <cassert>

namespace filurvst {
namespace gui {

bool get_font(const VSTGUI::IUIDescription* description,
              const std::string* font_name,
              VSTGUI::CFontRef& font) {
  assert(nullptr != description);

  bool ret_val = false;
  VSTGUI::CFontRef local_font = nullptr;
  if (nullptr != font_name) {
    local_font = description->getFont(font_name->c_str());
  }
  if (nullptr != local_font) {
    font = local_font;
    ret_val = true;
  }

  return ret_val;
}

bool get_string_attribute(const VSTGUI::UIAttributes& attributes,
                          const std::string& name,
                          std::string& value) {
  bool ret_val = false;
  const std::string* attribute = attributes.getAttributeValue(name);
  if (nullptr != attribute) {
    value = *attribute;
    ret_val = true;
  }

  return ret_val;
}

bool string_to_color(const VSTGUI::IUIDescription* description,
                     const std::string* value,
                     VSTGUI::CColor& color) {
  assert(nullptr != description);

  bool ret_val = false;

  if ((nullptr != value) && *value == "") {
    color = VSTGUI::kTransparentCColor;
    ret_val = true;
  }

  if ((false == ret_val) && (nullptr != value)) {
    ret_val = description->getColor(value->c_str(), color);
  }

  return ret_val;
}

}  // namespace gui
}  // namespace filurvst
