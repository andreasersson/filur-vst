/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutables/layoutable_knob.h>

namespace filurvst {
namespace gui {

const double LayoutableKnob::s_knob_range = 200.0f;

LayoutableKnob::LayoutableKnob(VSTGUI::IControlListener* listener,
                               int32_t tag,
                               std::unique_ptr<Drawable> drawable)
    : Layoutable(listener, tag, std::move(drawable)),
      m_startAngle(3.0 * M_PI / 4.0),
      m_rangeAngle(3.0 * M_PI / 2.0),
      m_zoomFactor(1.5),
      m_force_linear_mode(false),
      m_circular_mode_value(0.0),
      m_mouse_down_value(0.0),
      m_linear_mode_scale(0.0),
      m_knob_mode(VSTGUI::kLinearMode) {}

VSTGUI::CMouseEventResult LayoutableKnob::onMouseDown(VSTGUI::CPoint& where,
                                                      const VSTGUI::CButtonState& buttons) {
  if (!buttons.isLeftButton()) {
    return VSTGUI::kMouseEventNotHandled;
  }

  beginEdit();

  if (checkDefaultValue(buttons)) {
    endEdit();
    return VSTGUI::kMouseDownEventHandledButDontNeedMovedOrUpEvents;
  }

  m_mouse_down_point = where;
  m_previous_point = where;

  m_mouse_down_value = value;

  m_previous_button = buttons;

  VSTGUI::CKnobMode frame_mode = static_cast<VSTGUI::CKnobMode>(getFrame()->getKnobMode());
  if (m_force_linear_mode) {
    frame_mode = VSTGUI::kLinearMode;
  }

  switch (frame_mode) {
    case VSTGUI::kLinearMode:
      if ((buttons & VSTGUI::kAlt)) {
        m_knob_mode = VSTGUI::kCircularMode;
      } else {
        m_knob_mode = VSTGUI::kLinearMode;
      }
      break;

    default:
      if ((buttons & VSTGUI::kAlt)) {
        m_knob_mode = VSTGUI::kLinearMode;
      } else {
        m_knob_mode = frame_mode;
      }
      break;
  }

  if (VSTGUI::kLinearMode == m_knob_mode) {
    double range = s_knob_range;
    if (buttons & kZoomModifier) {
      range *= m_zoomFactor;
    }
    m_linear_mode_scale = (getMax() - getMin()) / range;
  } else if (VSTGUI::kCircularMode == m_knob_mode) {
    VSTGUI::CPoint tmp_point(where);
    tmp_point.offset(-getViewSize().left, -getViewSize().top);
    m_circular_mode_value = valueFromPoint(tmp_point, m_rangeAngle, m_startAngle);
  } else {
    VSTGUI::CPoint tmp_point(where);
    tmp_point.offset(-getViewSize().left, -getViewSize().top);
    m_circular_mode_value = valueFromPoint(tmp_point, 2 * M_PI, 0);
  }

  return onMouseMoved(where, buttons);
}

VSTGUI::CMouseEventResult LayoutableKnob::onMouseUp(VSTGUI::CPoint& where,
                                                    const VSTGUI::CButtonState& buttons) {
  (void) where;
  (void) buttons;

  endEdit();

  return VSTGUI::kMouseEventHandled;
}

VSTGUI::CMouseEventResult LayoutableKnob::onMouseMoved(VSTGUI::CPoint& where,
                                                       const VSTGUI::CButtonState& buttons) {
  (void) where;
  (void) buttons;

  if (!isEditing()) {
    return VSTGUI::kMouseEventNotHandled;
  }

  if (where == m_previous_point) {
    return VSTGUI::kMouseEventHandled;
  }

  m_previous_point = where;

  if (VSTGUI::kLinearMode == m_knob_mode) {
    VSTGUI::CCoord diff = (m_mouse_down_point.y - where.y) + (where.x - m_mouse_down_point.x);
    value = m_mouse_down_value + diff * m_linear_mode_scale;
    bounceValue();
  } else if (VSTGUI::kCircularMode == m_knob_mode) {
    double middle = (getMax() - getMin()) * 0.5f;
    VSTGUI::CPoint tmp_point(where);
    tmp_point.offset(-getViewSize().left, -getViewSize().top);
    value = valueFromPoint(tmp_point, m_rangeAngle, m_startAngle);

    if (m_circular_mode_value - value > middle) {
      value = getMax();
    } else if (value - m_circular_mode_value > middle) {
      value = getMin();
    } else {
      m_circular_mode_value = value;
    }
  } else /* kRelativCircularMode */ {
    double middle = (getMax() - getMin()) * 0.5f;
    VSTGUI::CPoint tmp_point(where);
    tmp_point.offset(-getViewSize().left, -getViewSize().top);
    double value_from_point = valueFromPoint(tmp_point, 2 * M_PI, 0);

    double diff_value = value_from_point - m_circular_mode_value;
    if (fabs(diff_value) > middle) {
      diff_value = 0.0;
    }
    m_circular_mode_value = value_from_point;
    value = std::max(std::min(value + diff_value, 1.0), 0.0);
  }

  if (value != getOldValue()) {
    valueChanged();
  }

  if (isDirty()) {
    invalid();
  }

  return VSTGUI::kMouseEventHandled;
}

bool LayoutableKnob::onWheel(const VSTGUI::CPoint& where,
                             const VSTGUI::CMouseWheelAxis& axis,
                             const float& distance,
                             const VSTGUI::CButtonState& buttons) {
  (void) where;
  (void) axis;

  if (!getMouseEnabled()) {
    return false;
  }

  double v = getValueNormalized();
  if (buttons & kZoomModifier) {
    v += 0.1f * distance * wheelInc;
  } else {
    v += distance * wheelInc;
  }

  setValueNormalized(v);

  if (isDirty()) {
    invalid();
    beginEdit();
    valueChanged();
    endEdit();
  }

  return true;
}

int32_t LayoutableKnob::onKeyDown(VstKeyCode& keyCode) {
  int32_t ret_val = 0;
  double delta_value = 0.0f;
  double zoom_value = 1.0f;

  if (mapVstKeyModifier(keyCode.modifier) & kZoomModifier) {
    zoom_value = 0.1f;
  }

  switch (keyCode.virt) {
    case VKEY_DOWN:
    case VKEY_LEFT:
      delta_value = -zoom_value * wheelInc;
      ret_val = 1;
      break;

    case VKEY_UP:
    case VKEY_RIGHT:
      delta_value = zoom_value * wheelInc;
      ret_val = 1;
      break;

    default:
      ret_val = -1;
      break;
  }

  if (ret_val == 1) {
    double value = getValueNormalized() + delta_value;
    setValueNormalized(value);

    if (isDirty()) {
      invalid();
      beginEdit();
      valueChanged();
      endEdit();
    }

  }

  return ret_val;
}

void LayoutableKnob::setStartAngle(double val) {
  m_startAngle = val;
  setDirty();
}

void LayoutableKnob::setRangeAngle(double val) {
  m_rangeAngle = val;
  setDirty();
}

double LayoutableKnob::valueFromPoint(VSTGUI::CPoint &point,
                                      double range_angle,
                                      double start_angle) const {
  double v;
  double d = range_angle * 0.5;
  double a = start_angle + d;
  VSTGUI::CPoint center_point(getViewSize().getWidth() * 0.5, getViewSize().getHeight() * 0.5);
  double xradius = center_point.x;
  double yradius = center_point.y;

  double dx = (point.x - center_point.x) / xradius;
  double dy = (point.y - center_point.y) / yradius;

  double alpha = atan2(dy, dx) - a;
  while (alpha >= VSTGUI::Constants::pi) {
    alpha -= VSTGUI::Constants::double_pi;
  }
  while (alpha < -VSTGUI::Constants::pi) {
    alpha += VSTGUI::Constants::double_pi;
  }

  if (d < 0.0) {
    alpha = -alpha;
  }

  if (alpha > d) {
    v = getMax();
  } else if (alpha < -d) {
    v = getMin();
  } else {
    v = (0.5 + alpha / range_angle);
  }

  v *= (getMax() - getMin());

  return v;
}

}  // namespace gui
}  // namespace filurvst
