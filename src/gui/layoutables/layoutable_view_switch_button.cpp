/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutables/layoutable_view_switch_button.h>

namespace filurvst {
namespace gui {

LayoutableViewSwitchButton::LayoutableViewSwitchButton(VSTGUI::IControlListener* listener,
                                                       int32_t tag,
                                                       float button_value,
                                                       float min_value,
                                                       float max_value,
                                                       std::unique_ptr<Drawable> drawable)
: Layoutable(listener, tag, std::move(drawable)),
  m_button_value(button_value) {
  this->setMin(min_value);
  this->setMax(max_value);
}

VSTGUI::CMouseEventResult LayoutableViewSwitchButton::onMouseDown(VSTGUI::CPoint& /*where*/,
                                                                  const VSTGUI::CButtonState& buttons) {
  VSTGUI::CMouseEventResult ret_val = VSTGUI::kMouseEventNotImplemented;

  if (!buttons.isLeftButton()) {
    ret_val = VSTGUI::kMouseEventNotHandled;
  }

  if (VSTGUI::kMouseEventNotImplemented == ret_val) {
    ret_val = VSTGUI::kMouseEventHandled;
    beginEdit();
    if (m_button_value != getValue()) {
      setValue(m_button_value);
      valueChanged();
      if (isDirty()) {
        invalid();
      }
    }
  }

  return ret_val;
}

VSTGUI::CMouseEventResult LayoutableViewSwitchButton::onMouseUp(VSTGUI::CPoint& /*where*/,
                                                                const VSTGUI::CButtonState& /*buttons*/) {
  endEdit();

  return VSTGUI::kMouseEventHandled;
}

void LayoutableViewSwitchButton::updateModel(DrawableModel& model) {
  double value = 0.0;
  if (std::round(m_button_value) == std::round(getValue())) {
    value = 1.0;
  }
  model.value = value;
}

}  // namespace gui
}  // namespace filurvst
