/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutables/layoutable_on_off_button.h>

namespace filurvst {
namespace gui {

LayoutableOnOffButton::LayoutableOnOffButton(VSTGUI::IControlListener* listener,
                                             int32_t tag,
                                             std::unique_ptr<Drawable> drawable)
    : Layoutable(listener, tag, std::move(drawable)) {
  setWantsFocus(false);
}

VSTGUI::CMouseEventResult LayoutableOnOffButton::onMouseDown(VSTGUI::CPoint& /*where*/,
                                                             const VSTGUI::CButtonState& buttons) {
  if (!buttons.isLeftButton()) {
    return VSTGUI::kMouseEventNotHandled;
  }

  beginEdit();

  float value = getValueNormalized();
  if (value > 0.5f) {
    value = 0.0f;
  } else {
    value = 1.0f;
  }

  setValueNormalized(value);

  valueChanged();

  if (isDirty()) {
    invalid();
  }

  return VSTGUI::kMouseEventHandled;
}

VSTGUI::CMouseEventResult LayoutableOnOffButton::onMouseUp(VSTGUI::CPoint& /*where*/, const VSTGUI::CButtonState& /*buttons*/) {
  endEdit();

  return VSTGUI::kMouseEventHandled;
}

}  // namespace gui
}  // namespace filurvst
