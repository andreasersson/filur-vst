/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutables/layoutable_option_menu.h>

namespace filurvst {
namespace gui {

LayoutableOptionMenu::LayoutableOptionMenu(VSTGUI::IControlListener* listener,
                                           int32_t tag,
                                           std::unique_ptr<Drawable> drawable)
    : VSTGUI::COptionMenu(VSTGUI::CRect(0, 0, 0, 0), listener, tag), m_drawable(std::move(drawable)) {
  LayoutableOptionMenu::setViewSize(VSTGUI::CRect(0, 0, 0, 0));
  setWantsFocus(false);
}

void LayoutableOptionMenu::setViewSize(const VSTGUI::CRect& rect, bool invalid) {
  VSTGUI::COptionMenu::setViewSize(rect, invalid);
  setMouseableArea(rect);

  setDirty();
}

void LayoutableOptionMenu::draw(VSTGUI::CDrawContext *context) {
  VSTGUI::CMenuItem* item = getEntry(currentIndex);
  if (nullptr != item) {
    m_model.text = item->getTitle().data();
  }

  if (m_drawable) {
    m_drawable->draw(context, getViewSize(), m_model);
  }
  setDirty(false);
}

}  // namespace gui
}  // namespace filurvst
