/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutables/layoutable_text.h>

namespace filurvst {
namespace gui {

LayoutableText::LayoutableText(VSTGUI::IControlListener* listener,
                               int32_t tag,
                               std::unique_ptr<Drawable> drawable)
    : VSTGUI::CTextLabel(VSTGUI::CRect(0, 0, 0, 0)),
      m_drawable(std::move(drawable)) {
  setListener(listener);
  setTag(tag);
  setTransparency(true);
}

void LayoutableText::draw(VSTGUI::CDrawContext* context) {
  m_model.value = getValue();
  m_model.text = text;

  if (m_drawable) {
    m_drawable->draw(context, getViewSize(), m_model);
  }

  setDirty(false);
}

void LayoutableText::setViewSize(const VSTGUI::CRect& rect, bool invalid) {
  VSTGUI::CTextLabel::setViewSize(rect, invalid);
  setMouseableArea(rect);
  setDirty();
}

}  // namespace gui
}  // namespace filurvst
