/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurep is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/aboutbox.h>

#include <filurvst/gui/aboutbox_controller.h>

#include <base/source/fobject.h>

namespace filurvst {
namespace gui {

AboutBox::AboutBox(std::unique_ptr<Layout> layout,
                   const VSTGUI::CRect& rect,
                   const VSTGUI::CRect& margins) :
    LayoutableContainer(std::move(layout), rect, margins) {
  setVisible(false);
  setTransparency(true);
  setWantsFocus(true);
}

AboutBox::~AboutBox() {
  unref();
}

VSTGUI::CMouseEventResult AboutBox::onMouseDown(VSTGUI::CPoint& /*where*/,
                                                const VSTGUI::CButtonState& /*buttons*/) {
  return VSTGUI::kMouseEventHandled;
}

VSTGUI::CMouseEventResult AboutBox::onMouseMoved(VSTGUI::CPoint& /*where*/,
                                                 const VSTGUI::CButtonState& /*buttons*/) {
  return VSTGUI::kMouseEventHandled;
}

VSTGUI::CMouseEventResult AboutBox::onMouseUp(VSTGUI::CPoint& /*where*/,
                                              const VSTGUI::CButtonState& /*buttons*/) {
  close();

  return VSTGUI::kMouseEventHandled;
}

void PLUGIN_API AboutBox::update(FUnknown* /*changedUnknown*/, Steinberg::int32 message) {
  switch (message) {
    case kMsgOpen:
      open();
      break;

    case kMsgClose:
      close();
      break;

    default:
      break;
  }
}

void AboutBox::setController(Steinberg::FObject* controller) {
  unref();
  m_fobject = controller;
  ref();
}

void AboutBox::ref() {
  if (nullptr != m_fobject) {
    m_fobject->addRef();
    m_fobject->addDependent(this);
  }
}

void AboutBox::unref() {
  if (nullptr != m_fobject) {
    m_fobject->removeDependent(this);
    m_fobject->release();
    m_fobject = nullptr;
  }
}

void AboutBox::open() {
  VSTGUI::CViewContainer* parent = nullptr;
  if ((nullptr != getParentView()) && (nullptr != getParentView()->asViewContainer())) {
    parent = getParentView()->asViewContainer();
  }

  if (nullptr != parent) {
    // send to front
    parent->changeViewZOrder(this, parent->getNbViews() - 1);
    setWantsFocus(true);
    setVisible(true);
    takeFocus();
  }
}

void AboutBox::close() {
  VSTGUI::CViewContainer* parent = nullptr;
  setVisible(false);
  if ((nullptr != getParentView()) && (nullptr != getParentView()->asViewContainer())) {
    parent = getParentView()->asViewContainer();
  }

  if (nullptr != parent) {
    // send to back
    parent->changeViewZOrder(this, 0);
  }
}

void AboutBoxController::open() {
  changed(AboutBox::kMsgOpen);
}
void AboutBoxController::close() {
  changed(AboutBox::kMsgClose);
}

}  // namespace gui
}  // namespace filurvst
