/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layout_factory.h>

#include <filurvst/gui/layout.h>
#include <filurvst/gui/uidesc.h>

#include <filurvst/gui/layouts/dist_layout.h>
#include <filurvst/gui/layouts/fill_layout.h>
#include <filurvst/gui/layouts/grid_layout.h>
#include <filurvst/gui/layouts/layer_layout.h>
#include <filurvst/gui/layouts/list_layout.h>
#include <filurvst/gui/layouts/page_layout.h>
#include <filurvst/gui/layouts/parameter_layout.h>

#include <vstgui/uidescription/uiattributes.h>
#include <vstgui/uidescription/uidescription.h>
#include <vstgui/uidescription/detail/uiviewcreatorattributes.h>

#include <map>
#include <memory>

namespace filurvst {
namespace gui {

using namespace VSTGUI;
using layout_ptr = std::unique_ptr<Layout>;
using factory_func = std::function<layout_ptr(const UIAttributes& attributes, const IUIDescription* description)>;

static const std::string kAttrLayout = "layout";
static const std::string kAttrFillLayout = "fill";
static const std::string kAttrDistLayout = "distributed";
static const std::string kAttrListLayout = "list";
static const std::string kAttrGridLayout = "grid";
static const std::string kAttrLayerLayout = "layer";
static const std::string kAttrParameterLayout = "parameter";
static const std::string kAttrModuleLayout = "module";
static const std::string kAttrPageLayout = "page";

// Attributes
static const std::string kAttrFont = "font";
static const std::string kAttrSpacing = "spacing";
static const std::string kAttrOrientation = "orientation";
static const std::string kAttrHorizontalSpacing = "horizontal-spacing";
static const std::string kAttrVerticalSpacing = "vertical-spacing";
static const std::string kAttrNumRows = "rows";
static const std::string kAttrNumColumns = "columns";
static const std::string kAttrDistribution = "distribution";
static const std::string kAttrInset = "inset";
static const std::string kAttrHeaderHeight = "header-height";
static const std::string kAttrModuleHeight = "module-height";
static const std::string kAttrParameterScale = "scale";
static const std::string kAttrParameterSpacing = "parameter-spacing";

static const std::string kAttrPageLayoutSpacing = "page-layout-spacing";
static const std::string kAttrPageLayoutHeaderHeight = "page-layout-header-height";
static const std::string kAttrModuleLayoutSpacing = "module-layout-spacing";
static const std::string kAttrModuleLayoutHeaderHeight = "module-layout-header-height";
static const std::string kAttrModuleLayoutHeight = "module-layout-height";

static std::string kOrientationHorizontal = "horizontal";
static std::string kOrientationVertical = "vertical";

static void split(const std::string& string,
                  std::vector<double>& vector,
                  char delimiter = ',');
static void split(const std::string& string,
                  std::vector<int>& vector,
                  char delimiter = ',');

static layout_ptr fill_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description);
static layout_ptr list_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description);
static layout_ptr grid_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description);
static layout_ptr dist_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description);
static layout_ptr layer_layout_factory(const UIAttributes& attributes,
                                       const IUIDescription* description);
static layout_ptr parameter_layout_factory(const UIAttributes& attributes,
                                           const IUIDescription* description);
static layout_ptr page_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description);
static layout_ptr module_layout_factory(const UIAttributes& attributes,
                                        const IUIDescription* description);

static bool fill_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* description,
                              Layout* layout);
static bool list_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* description,
                              Layout* layout);
static bool grid_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* description,
                              Layout* layout);
static bool dist_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* description,
                              Layout* layout);
static bool layer_layout_apply(const UIAttributes& attributes,
                               const IUIDescription* description,
                               Layout* layout);
static bool parameter_layout_apply(const UIAttributes& attributes,
                                   const IUIDescription* description,
                                   Layout* layout);
static bool page_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* description,
                              Layout* layout);
static bool module_layout_apply(const UIAttributes& attributes,
                                const IUIDescription* description,
                                Layout* layout);

layout_ptr layout_factory(const UIAttributes& attributes, const IUIDescription* description) {
  static const std::map<std::string, factory_func> factory_map = {
      { kAttrFillLayout, fill_layout_factory },
      { kAttrListLayout, list_layout_factory },
      { kAttrGridLayout, grid_layout_factory },
      { kAttrDistLayout, dist_layout_factory },
      { kAttrLayerLayout, layer_layout_factory },
      { kAttrParameterLayout, parameter_layout_factory },
      { kAttrModuleLayout, module_layout_factory },
      { kAttrPageLayout, page_layout_factory } };

  layout_ptr layout;
  std::string layout_name;

  get_string_attribute(attributes, kAttrLayout, layout_name);
  if (factory_map.count(layout_name) > 0) {
    layout = factory_map.at(layout_name)(attributes, description);
  }

  return layout;
}

bool layout_apply(const UIAttributes& attributes,
                  const IUIDescription* description,
                  Layout* layout) {
  bool handled = false;

  if (!handled) {
    handled = fill_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = list_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = grid_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = dist_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = layer_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = parameter_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = page_layout_apply(attributes, description, layout);
  }
  if (!handled) {
    handled = module_layout_apply(attributes, description, layout);
  }

  return handled;
}

static layout_ptr fill_layout_factory(const UIAttributes& /*attributes*/,
                                      const IUIDescription* /*description*/) {
  layout_ptr layout(new FillLayout());

  return layout;
}

static layout_ptr list_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description) {
  double spacing = 0.0;
  ListLayout::Orientation orientation = ListLayout::kHorizontal;
  std::string value;

  description->getVariable(kAttrSpacing.c_str(), spacing);
  attributes.getDoubleAttribute(kAttrSpacing, spacing);

  get_string_attribute(attributes, kAttrOrientation, value);
  if (kOrientationVertical == value) {
    orientation = ListLayout::kVertical;
  }

  layout_ptr layout(new ListLayout(orientation, spacing));

  return layout;
}

static layout_ptr grid_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description) {
  double horizontal_spacing = 0.0;
  double vertical_spacing = 0.0;
  int32_t number_of_columns = 1;
  int32_t number_of_rows = 1;

  description->getVariable(kAttrHorizontalSpacing.c_str(), horizontal_spacing);
  description->getVariable(kAttrVerticalSpacing.c_str(), vertical_spacing);
  attributes.getDoubleAttribute(kAttrHorizontalSpacing, horizontal_spacing);
  attributes.getDoubleAttribute(kAttrVerticalSpacing, vertical_spacing);

  attributes.getIntegerAttribute(kAttrNumRows, number_of_rows);
  attributes.getIntegerAttribute(kAttrNumColumns, number_of_columns);

  layout_ptr layout(new GridLayout(number_of_columns, number_of_rows, horizontal_spacing, vertical_spacing));

  return layout;
}

static layout_ptr dist_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description) {
  double spacing = 0.0;
  DistLayout::Orientation orientation = DistLayout::kHorizontal;
  std::string value;

  description->getVariable(kAttrSpacing.c_str(), spacing);
  attributes.getDoubleAttribute(kAttrSpacing, spacing);

  get_string_attribute(attributes, kAttrOrientation, value);
  if (kOrientationVertical == value) {
    orientation = DistLayout::kVertical;
  }

  std::vector<int> distribution;
  get_string_attribute(attributes, kAttrDistribution, value);
  if (!value.empty()) {
    split(value, distribution);
  }

  layout_ptr layout(new DistLayout(distribution, orientation, spacing));

  return layout;
}

static layout_ptr layer_layout_factory(const UIAttributes& attributes,
                                       const IUIDescription* /*description*/) {
  std::string value;
  std::vector<double> inset;

  get_string_attribute(attributes, kAttrInset, value);
  split(value, inset);

  layout_ptr layout(new LayerLayout(inset));

  return layout;
}

static layout_ptr parameter_layout_factory(const UIAttributes& attributes,
                                           const IUIDescription* description) {
  static const std::string kFontParameter = "parameter";
  double scale = 1.0;
  double spacing = 0.0;
  CFontRef font = nullptr;

  get_font(description, &kFontParameter, font);
  get_font(description, attributes.getAttributeValue(kAttrFont), font);
  attributes.getDoubleAttribute(kAttrParameterScale, scale);
  description->getVariable(kAttrParameterSpacing.c_str(), spacing);
  attributes.getDoubleAttribute(kAttrSpacing, spacing);

  layout_ptr layout(new ParameterLayout(font, scale, spacing));

  return layout;
}

static layout_ptr page_layout_factory(const UIAttributes& attributes,
                                      const IUIDescription* description) {
  double spacing = 0.0;
  double header_height = 0.0;

  description->getVariable(kAttrPageLayoutSpacing.c_str(), spacing);
  description->getVariable(kAttrPageLayoutHeaderHeight.c_str(), header_height);

  attributes.getDoubleAttribute(kAttrSpacing, spacing);
  attributes.getDoubleAttribute(kAttrHeaderHeight, header_height);

  layout_ptr layout(new PageLayout(header_height, spacing));

  return layout;
}

static layout_ptr module_layout_factory(const UIAttributes& attributes,
                                        const IUIDescription* description) {
  double module_spacing = 0.0;
  double module_header_height = 0.0;
  double module_height = 0.0;

  description->getVariable(kAttrModuleLayoutSpacing.c_str(), module_spacing);
  description->getVariable(kAttrModuleLayoutHeaderHeight.c_str(), module_header_height);
  description->getVariable(kAttrModuleLayoutHeight.c_str(), module_height);

  attributes.getDoubleAttribute(kAttrSpacing, module_spacing);
  attributes.getDoubleAttribute(kAttrHeaderHeight, module_header_height);
  attributes.getDoubleAttribute(kAttrModuleHeight, module_height);

  double sum = module_spacing + module_header_height + module_height;
  double header_height = module_header_height / sum;
  double spacing = module_spacing / sum;

  layout_ptr layout(new PageLayout(header_height, spacing));

  return layout;
}

static bool fill_layout_apply(const UIAttributes& /*attributes*/,
                              const IUIDescription* /*description*/,
                              Layout* layout) {
  bool ret_val = false;
  FillLayout* fill = dynamic_cast<FillLayout*>(layout);
  if (nullptr != fill) {
    ret_val = true;
  }

  return ret_val;
}

static bool list_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* /*description*/,
                              Layout* layout) {
  bool ret_val = false;

  ListLayout* list = dynamic_cast<ListLayout*>(layout);
  if (nullptr != list) {
    attributes.getDoubleAttribute(kAttrSpacing, list->spacing);
    std::string value;
    if (get_string_attribute(attributes, kAttrOrientation, value)) {
      if (kOrientationVertical == value) {
        list->orientation = ListLayout::kVertical;
      } else if (kOrientationHorizontal == value) {
        list->orientation = ListLayout::kHorizontal;
      }
    }

    ret_val = true;
  }

  return ret_val;
}

static bool grid_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* /*description*/,
                              Layout* layout) {
  bool ret_val = false;
  GridLayout* grid = dynamic_cast<GridLayout*>(layout);
  if (nullptr != grid) {
    attributes.getDoubleAttribute(kAttrHorizontalSpacing, grid->h_spacing);
    attributes.getDoubleAttribute(kAttrVerticalSpacing, grid->v_spacing);
    attributes.getIntegerAttribute(kAttrNumRows, grid->number_of_rows);
    attributes.getIntegerAttribute(kAttrNumColumns, grid->number_of_columns);

    ret_val = true;
  }

  return ret_val;
}

static bool dist_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* /*description*/,
                              Layout* layout) {
  bool ret_val = false;

  DistLayout* dist = dynamic_cast<DistLayout*>(layout);
  if (nullptr != dist) {
    attributes.getDoubleAttribute(kAttrSpacing, dist->spacing);
    std::string value;
    get_string_attribute(attributes, kAttrOrientation, value);
    if (kOrientationVertical == value) {
      dist->orientation = DistLayout::kVertical;
    } else if (kOrientationHorizontal == value) {
      dist->orientation = DistLayout::kHorizontal;
    }

    if (get_string_attribute(attributes, kAttrDistribution, value)) {
      std::vector<int> distribution;
      split(value, distribution);
      if (distribution.size() > 0) {
        dist->distribution = distribution;
      }
    }

    ret_val = true;
  }

  return ret_val;
}

static bool layer_layout_apply(const UIAttributes& attributes,
                               const IUIDescription* /*description*/,
                               Layout* layout) {
  bool ret_val = false;

  LayerLayout* layer = dynamic_cast<LayerLayout*>(layout);
  if (nullptr != layer) {
    std::string value;
    if (get_string_attribute(attributes, kAttrInset, value)) {
      std::vector<double> inset;
      split(value, inset);
      if (inset.size() > 0) {
        layer->inset = inset;
      }
    }
    ret_val = true;
  }

  return ret_val;
}

static bool parameter_layout_apply(const UIAttributes& attributes,
                                   const IUIDescription* description,
                                   Layout* layout) {
  bool ret_val = false;

  ParameterLayout* parameter = dynamic_cast<ParameterLayout*>(layout);
  if (nullptr != parameter) {
    CFontRef font = nullptr;
    get_font(description, attributes.getAttributeValue(kAttrFont), font);
    if (nullptr != font) {
      parameter->label_font = font;
    }
    attributes.getDoubleAttribute(kAttrParameterScale, parameter->control_scale);
    attributes.getDoubleAttribute(kAttrSpacing, parameter->spacing);

    ret_val = true;
  }

  return ret_val;
}

static bool page_layout_apply(const UIAttributes& attributes,
                              const IUIDescription* /*description*/,
                              Layout* layout) {
  bool ret_val = true;
  PageLayout* page = nullptr;
  std::string layout_name;

  get_string_attribute(attributes, kAttrLayout, layout_name);
  if (kAttrPageLayout != layout_name) {
    ret_val = false;
  }

  if (ret_val) {
    page = dynamic_cast<PageLayout*>(layout);
    if (nullptr == page) {
      ret_val = false;
    }
  }

  if (ret_val) {
    attributes.getDoubleAttribute(kAttrSpacing, page->spacing);
    attributes.getDoubleAttribute(kAttrHeaderHeight, page->header_height);
  }

  return ret_val;
}

static bool module_layout_apply(const UIAttributes& attributes,
                                const IUIDescription* /*description*/,
                                Layout* layout) {
  bool ret_val = true;
  PageLayout* page = nullptr;
  std::string layout_name;

  get_string_attribute(attributes, kAttrLayout, layout_name);
  if (kAttrModuleLayout != layout_name) {
    ret_val = false;
  }

  if (ret_val) {
    page = dynamic_cast<PageLayout*>(layout);
    if (nullptr == page) {
      ret_val = false;
    }
  }

  if (ret_val) {
    double spacing = 0.0;
    double header_height = 0.0;
    double height = 0.0;
    bool attributes_found = true;
    attributes_found &= attributes.getDoubleAttribute(kAttrSpacing, spacing);
    attributes_found &= attributes.getDoubleAttribute(kAttrHeaderHeight, header_height);
    attributes_found &= attributes.getDoubleAttribute(kAttrModuleHeight, height);

    if (attributes_found) {
      double sum = spacing + header_height + height;
      page->spacing = spacing / sum;
      page->header_height = header_height / sum;
    }
  }

  return ret_val;
}

static void split(const std::string& string,
                  std::vector<double>& vector,
                  char delimiter) {
  std::stringstream stream(string);
  std::string item;
  while (std::getline(stream, item, delimiter)) {
    if (!item.empty()) {
      vector.push_back(std::stod(item));
    }
  }
}

static void split(const std::string& string,
                  std::vector<int>& vector,
                  char delimiter) {
  std::stringstream stream(string);
  std::string item;
  while (std::getline(stream, item, delimiter)) {
    if (!item.empty()) {
      vector.push_back(std::stol(item));
    }
  }
}

}  // namespace gui
}  // namespace filurvst
