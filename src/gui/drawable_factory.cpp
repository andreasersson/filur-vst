/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/drawable_factory.h>

#include <filurvst/gui/uidesc.h>

#include <filurvst/gui/drawable.h>
#include <filurvst/gui/drawables/alpha_rect_drawable.h>
#include <filurvst/gui/drawables/knob_drawable.h>
#include <filurvst/gui/drawables/on_off_button_drawable.h>
#include <filurvst/gui/drawables/rect_drawable.h>
#include <filurvst/gui/drawables/slider_drawable.h>
#include <filurvst/gui/drawables/text_drawable.h>
#include <filurvst/gui/drawables/text_rect_drawable.h>

#include <vstgui/uidescription/uiattributes.h>
#include <vstgui/uidescription/uidescription.h>

#include <map>
#include <memory>

namespace filurvst {
namespace gui {

using namespace VSTGUI;
using drawable_ptr = std::unique_ptr<Drawable>;
using factory_func = std::function<drawable_ptr(const UIAttributes& attributes, const IUIDescription* description)>;

static const std::string kRectDrawable("rect");
static const std::string kRectAlphaDrawable("alpha-rect");
static const std::string kKnobDrawable("knob");
static const std::string kSliderDrawble("slider");
static const std::string kTextDrawable("text");
static const std::string kTextRectDrawable("text-rect");
static const std::string kOnOffButtonDrawable("on-off-button");

static const std::string kAttrDrawable = "drawable";

static const std::string kAttrColor = "color";
static const std::string kAttrBgColor = "background-color";
static const std::string kAttrTextColor = "text-color";
static const std::string kAttrActiveColor = "active-color";
static const std::string kAttrInactiveColor = "inactive-color";

static const std::string kAttrButtonColor = "color";
static const std::string kAttrButtonOnColor = "on-color";
static const std::string kAttrButtonOffColor = "off-color";
static const std::string kAttrTextRectActiveColor = "text-rect-active-color";
static const std::string kAttrTextRectInactiveColor = "text-rect-inactive-color";
static const std::string kAttrTextRectColor = "text-rect-color";

static const std::string kAttrButtonOnText = "button-on-text";
static const std::string kAttrButtonOffText = "button-off-text";

static const std::string kAttrText = "text";
static const std::string kAttrRadius = "radius";
static const std::string kAttrAspectRatio = "aspect-ratio";
static const std::string kAttrButtonRadius = "button-radius";
static const std::string kAttrButtonAspectRatio = "button-aspect-ratio";
static const std::string kAttrTextRectRadius = "text-rect-radius";
static const std::string kAttrTextRectAspectRatio = "text-rect-aspect-ratio";
static const std::string kAttrSymmetric = "symmetric";
static const std::string kAttrOrientation = "orientation";
static const std::string kOrientationHorizontal = "horizontal";
static const std::string kOrientationVertical = "vertical";
static const std::string kAttrFont = "font";

// Default fonts
static const std::string kFontParameter = "parameter";
static const std::string kFontText = "text";
static const std::string kFontTextRect = "text-rect";

static drawable_ptr rect_factory(const UIAttributes& attributes,
                                 const IUIDescription* description);
static drawable_ptr alpha_rect_factory(const UIAttributes& attributes,
                                       const IUIDescription* description);
static drawable_ptr knob_factory(const UIAttributes& attributes,
                                 const IUIDescription* description);
static drawable_ptr slider_factory(const UIAttributes& attributes,
                                   const IUIDescription* description);
static drawable_ptr on_off_button_factory(const UIAttributes& attributes,
                                          const IUIDescription* description);
static drawable_ptr text_factory(const UIAttributes& attributes,
                                 const IUIDescription* description);
static drawable_ptr text_rect_factory(const UIAttributes& attributes,
                                      const IUIDescription* description);

static bool rect_apply(const UIAttributes& attributes,
                       const IUIDescription* description,
                       Drawable* drawable);
static bool alpha_rect_apply(const UIAttributes& attributes,
                             const IUIDescription* description,
                             Drawable* drawable);
static bool knob_apply(const UIAttributes& attributes,
                       const IUIDescription* description,
                       Drawable* drawable);
static bool slider_apply(const UIAttributes& attributes,
                         const IUIDescription* description,
                         Drawable* drawable);
static bool on_off_button_apply(const UIAttributes& attributes,
                                const IUIDescription* description,
                                Drawable* drawable);
static bool text_apply(const UIAttributes& attributes,
                       const IUIDescription* description,
                       Drawable* drawable);
static bool text_rect_apply(const UIAttributes& attributes,
                            const IUIDescription* description,
                            Drawable* drawable);

drawable_ptr drawable_factory(const UIAttributes& attributes,
                              const IUIDescription* description) {
  drawable_ptr drawable;

  static const std::map<std::string, factory_func> factory_map = {
      { kRectDrawable, rect_factory },
      { kRectAlphaDrawable, alpha_rect_factory },
      { kKnobDrawable, knob_factory },
      { kSliderDrawble, slider_factory },
      { kTextDrawable, text_factory },
      { kTextRectDrawable, text_rect_factory },
      { kOnOffButtonDrawable, on_off_button_factory } };

  const std::string* attribute = attributes.getAttributeValue(kAttrDrawable);
  if (nullptr != attribute) {
    if (factory_map.count(*attribute) > 0) {
      drawable = factory_map.at(*attribute)(attributes, description);
    }
  }

  return drawable;
}

bool drawable_apply(const UIAttributes& attributes,
                    const IUIDescription* description,
                    Drawable* drawable) {
  bool handled = false;

  if (!handled) {
    handled = rect_apply(attributes, description, drawable);
  }
  if (!handled) {
    handled = alpha_rect_apply(attributes, description, drawable);
  }
  if (!handled) {
    handled = knob_apply(attributes, description, drawable);
  }
  if (!handled) {
    handled = slider_apply(attributes, description, drawable);
  }
  if (!handled) {
    handled = text_apply(attributes, description, drawable);
  }
  if (!handled) {
    handled = text_rect_apply(attributes, description, drawable);
  }
  if (!handled) {
    handled = on_off_button_apply(attributes, description, drawable);
  }

  return handled;
}

static drawable_ptr rect_factory(const UIAttributes& attributes,
                                 const IUIDescription* description) {
  drawable_ptr drawable;

  CColor color;
  double radius = 0.0;

  string_to_color(description, &kAttrColor, color);
  string_to_color(description, attributes.getAttributeValue(kAttrColor), color);

  description->getVariable(kAttrButtonRadius.c_str(), radius);
  attributes.getDoubleAttribute(kAttrRadius, radius);

  drawable.reset(new RectDrawable(color, radius));

  return drawable;
}

static drawable_ptr alpha_rect_factory(const UIAttributes& attributes,
                                       const IUIDescription* description) {
  CColor color;
  CColor bg_color = kTransparentCColor;
  double radius = 0.0;

  string_to_color(description, &kAttrActiveColor, color);
  string_to_color(description, &kAttrColor, color);
  string_to_color(description, attributes.getAttributeValue(kAttrColor), color);

  string_to_color(description, attributes.getAttributeValue(kAttrBgColor), bg_color);

  description->getVariable(kAttrButtonRadius.c_str(), radius);
  attributes.getDoubleAttribute(kAttrRadius, radius);
  drawable_ptr drawable(new AlphaRectDrawable(color, bg_color, radius));

  return drawable;
}

static drawable_ptr knob_factory(const UIAttributes& attributes,
                                 const IUIDescription* description) {
  drawable_ptr drawable;

  CColor active_color;
  CColor inactive_color;

  string_to_color(description, &kAttrActiveColor, active_color);
  string_to_color(description, attributes.getAttributeValue(kAttrActiveColor), active_color);

  string_to_color(description, &kAttrInactiveColor, inactive_color);
  string_to_color(description, attributes.getAttributeValue(kAttrInactiveColor), inactive_color);

  bool symmetric = false;
  attributes.getBooleanAttribute(kAttrSymmetric, symmetric);

  drawable.reset(new KnobDrawable(active_color, inactive_color, symmetric));

  return drawable;
}

static drawable_ptr slider_factory(const UIAttributes& attributes,
                                   const IUIDescription* description) {
  drawable_ptr drawable;

  CColor active_color;
  CColor inactive_color;

  string_to_color(description, &kAttrActiveColor, active_color);
  string_to_color(description, attributes.getAttributeValue(kAttrActiveColor), active_color);

  string_to_color(description, &kAttrInactiveColor, inactive_color);
  string_to_color(description, attributes.getAttributeValue(kAttrInactiveColor), inactive_color);

  bool vertical = true;
  std::string value;
  get_string_attribute(attributes, kAttrOrientation, value);
  if (kOrientationHorizontal == value) {
    vertical = false;
  } else if (kOrientationVertical == value) {
    vertical = true;
  }

  if (vertical) {
    drawable.reset(new VerticalSliderDrawable(active_color, inactive_color));
  }

  return drawable;
}
static drawable_ptr on_off_button_factory(const UIAttributes& attributes, const IUIDescription* description) {
  CColor off_color;
  CColor on_color;
  CColor button_color;
  std::string on_text = "on";
  std::string off_text = "off";
  CFontRef font = nullptr;
  double radius = 0.0;
  double aspect_ratio = 1.0;

  description->getVariable(kAttrButtonRadius.c_str(), radius);
  attributes.getDoubleAttribute(kAttrRadius, radius);

  description->getVariable(kAttrButtonAspectRatio.c_str(), aspect_ratio);
  attributes.getDoubleAttribute(kAttrAspectRatio, aspect_ratio);

  get_font(description, &kFontParameter, font);
  get_font(description, attributes.getAttributeValue(kAttrFont), font);

  string_to_color(description, &kAttrTextColor, off_color);
  string_to_color(description, attributes.getAttributeValue(kAttrButtonOffColor), off_color);

  string_to_color(description, &kAttrActiveColor, on_color);
  string_to_color(description, attributes.getAttributeValue(kAttrButtonOnColor), on_color);

  string_to_color(description, &kAttrInactiveColor, button_color);
  string_to_color(description, attributes.getAttributeValue(kAttrButtonColor), button_color);

  get_string_attribute(attributes, kAttrButtonOnText, on_text);
  get_string_attribute(attributes, kAttrButtonOffText, off_text);

  drawable_ptr drawable(new OnOffButtonDrawable(on_text, off_text, on_color, off_color, button_color, font, radius, aspect_ratio));

  return drawable;
}

static drawable_ptr text_factory(const UIAttributes& attributes,
                                 const IUIDescription* description) {
  std::string text;
  CColor text_color;
  CFontRef font = nullptr;

  get_string_attribute(attributes, kAttrText, text);

  get_font(description, &kFontText, font);
  get_font(description, attributes.getAttributeValue(kAttrFont), font);

  string_to_color(description, &kAttrTextColor, text_color);
  string_to_color(description, attributes.getAttributeValue(kAttrTextColor), text_color);

  drawable_ptr drawable(new TextDrawable(text, text_color, font));

  return drawable;
}

static drawable_ptr text_rect_factory(const UIAttributes& attributes,
                                      const IUIDescription* description) {
  CColor inactive_color;
  CColor active_color;
  CColor text_color;
  double radius = 0.0;
  double aspect_ratio = 1.0;
  std::string text;
  CFontRef font = nullptr;

  description->getVariable(kAttrTextRectRadius.c_str(), radius);
  attributes.getDoubleAttribute(kAttrRadius, radius);

  description->getVariable(kAttrTextRectAspectRatio.c_str(), aspect_ratio);
  attributes.getDoubleAttribute(kAttrAspectRatio, aspect_ratio);

  string_to_color(description, &kAttrInactiveColor, inactive_color);
  string_to_color(description, &kAttrTextRectInactiveColor, inactive_color);
  string_to_color(description, attributes.getAttributeValue(kAttrInactiveColor), inactive_color);

  string_to_color(description, &kAttrActiveColor, active_color);
  string_to_color(description, &kAttrTextRectActiveColor, active_color);
  string_to_color(description, attributes.getAttributeValue(kAttrActiveColor), active_color);

  string_to_color(description, &kAttrTextColor, text_color);
  string_to_color(description, &kAttrTextRectColor, text_color);
  string_to_color(description, attributes.getAttributeValue(kAttrTextColor), text_color);

  get_font(description, &kFontText, font);
  get_font(description, &kFontTextRect, font);
  get_font(description, attributes.getAttributeValue(kAttrFont), font);

  get_string_attribute(attributes, kAttrText, text);

  drawable_ptr drawable(new TextRectDrawable(text, text_color, inactive_color, active_color, font, radius, aspect_ratio));

  return drawable;
}

static bool rect_apply(const UIAttributes& attributes,
                       const IUIDescription* description, Drawable* drawable) {
  bool ret_val = false;

  RectDrawable* rect = dynamic_cast<RectDrawable*>(drawable);
  if (nullptr != rect) {
    string_to_color(description, attributes.getAttributeValue(kAttrColor), rect->color);
    attributes.getDoubleAttribute(kAttrRadius, rect->radius);

    ret_val = true;
  }

  return ret_val;
}

static bool alpha_rect_apply(const UIAttributes& attributes,
                             const IUIDescription* description,
                             Drawable* drawable) {
  bool ret_val = false;

  AlphaRectDrawable* rect = dynamic_cast<AlphaRectDrawable*>(drawable);
  if (nullptr != rect) {
    string_to_color(description, attributes.getAttributeValue(kAttrColor), rect->color);
    string_to_color(description, attributes.getAttributeValue(kAttrBgColor), rect->bg_color);
    attributes.getDoubleAttribute(kAttrRadius, rect->radius);

    ret_val = true;
  }

  return ret_val;
}

static bool knob_apply(const UIAttributes& attributes,
                       const IUIDescription* description, Drawable* drawable) {
  bool ret_val = false;
  KnobDrawable* knob = dynamic_cast<KnobDrawable*>(drawable);
  if (nullptr != knob) {
    string_to_color(description, attributes.getAttributeValue(kAttrActiveColor), knob->color);
    string_to_color(description, attributes.getAttributeValue(kAttrInactiveColor), knob->bg_color);

    bool symmetric = false;
    if (attributes.getBooleanAttribute(kAttrSymmetric, symmetric)) {
      knob->setSymmetric(symmetric);
    }

    ret_val = true;
  }

  return ret_val;
}

static bool slider_apply(const UIAttributes& attributes,
                         const IUIDescription* description,
                         Drawable* drawable) {
  bool ret_val = false;

  VerticalSliderDrawable* slider =
      dynamic_cast<VerticalSliderDrawable*>(drawable);
  if (nullptr != slider) {
    string_to_color(description, attributes.getAttributeValue(kAttrActiveColor), slider->color);
    string_to_color(description, attributes.getAttributeValue(kAttrInactiveColor), slider->bg_color);

    ret_val = true;
  }

  return ret_val;
}

static bool on_off_button_apply(const UIAttributes& attributes,
                                const IUIDescription* description,
                                Drawable* drawable) {
  bool ret_val = false;

  OnOffButtonDrawable* button = dynamic_cast<OnOffButtonDrawable*>(drawable);
  if (nullptr != button) {
    attributes.getDoubleAttribute(kAttrRadius, button->radius);
    get_font(description, attributes.getAttributeValue(kAttrFont), button->font);
    string_to_color(description, attributes.getAttributeValue(kAttrButtonOffColor), button->off_color);
    string_to_color(description, attributes.getAttributeValue(kAttrButtonOnColor), button->on_color);
    string_to_color(description, attributes.getAttributeValue(kAttrButtonColor), button->bg_color);
    get_string_attribute(attributes, kAttrButtonOnText, button->on_text);
    get_string_attribute(attributes, kAttrButtonOffText, button->off_text);

    ret_val = true;
  }

  return ret_val;
}

static bool text_apply(const UIAttributes& attributes,
                       const IUIDescription* description, Drawable* drawable) {
  bool ret_val = false;

  TextDrawable* text = dynamic_cast<TextDrawable*>(drawable);
  if (nullptr != text) {
    get_string_attribute(attributes, kAttrText, text->text);
    get_font(description, attributes.getAttributeValue(kAttrFont), text->font);
    string_to_color(description, attributes.getAttributeValue(kAttrTextColor), text->color);

    ret_val = true;
  }

  return ret_val;
}

static bool text_rect_apply(const UIAttributes& attributes,
                            const IUIDescription* description,
                            Drawable* drawable) {
  bool ret_val = false;

  TextRectDrawable* text = dynamic_cast<TextRectDrawable*>(drawable);
  if (nullptr != text) {
    attributes.getDoubleAttribute(kAttrRadius, text->radius);
    get_font(description, attributes.getAttributeValue(kAttrFont), text->font);
    string_to_color(description, attributes.getAttributeValue(kAttrTextColor), text->font_color);
    string_to_color(description, attributes.getAttributeValue(kAttrButtonColor), text->bg_color);

    ret_val = true;
  }

  return ret_val;
}

}  // namespace gui
}  // namespace filurvst
