/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/layer_layout.h>

#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

void LayerLayout::layout(LayoutableContainer& layoutable) {
  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = rect.getWidth() - margins.left - margins.right;
  double height = rect.getHeight() - margins.top - margins.bottom;
  double local_inset = 0.0;

  for (uint32_t index = 0; index < layoutable.getNbViews(); ++index) {
    if (index < inset.size()) {
      local_inset = inset.at(index);
    }

    double view_width = std::max(0.0, width - 2 * local_inset);
    double view_height = std::max(0.0, height - 2 * local_inset);
    double x = margins.left + local_inset;
    double y = margins.top + local_inset;

    VSTGUI::CRect view_rect(x, y, x + view_width, y + view_height);

    VSTGUI::CView *view = layoutable.getView(index);
    if (nullptr != view) {
      view->setViewSize(view_rect);
      view->setMouseableArea(view_rect);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
