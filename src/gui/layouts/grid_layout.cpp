/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/grid_layout.h>

#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

void GridLayout::layout(LayoutableContainer& layoutable) {
  int32_t num_views = layoutable.getNbViews();
  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = rect.getWidth() - margins.left - margins.right;
  double height = rect.getHeight() - margins.top - margins.bottom;

  double view_width = width
      / (number_of_columns + (number_of_columns - 1) * h_spacing);
  double view_height = height
      / (number_of_rows + (number_of_rows - 1) * v_spacing);
  double local_h_spacing = view_width * h_spacing;
  double local_v_spacing = view_height * v_spacing;

  int row = 0;
  int column = 0;

  for (int child_index = 0; child_index < num_views; ++child_index) {
    double x = margins.left + view_width * column + local_h_spacing * column;
    double y = margins.top + view_height * row + local_v_spacing * row;
    VSTGUI::CRect view_rect(x, y, x + view_width, y + view_height);

    ++column;
    if (column >= number_of_columns) {
      column = 0;
      ++row;
    }

    VSTGUI::CView *view = layoutable.getView(child_index);
    if (nullptr != view) {
      view->setViewSize(view_rect);
      view->setMouseableArea(view_rect);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
