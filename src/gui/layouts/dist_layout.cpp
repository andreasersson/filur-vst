/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/dist_layout.h>

#include <filurvst/gui/layoutable_container.h>

#include <numeric>

namespace filurvst {
namespace gui {

void DistLayout::layout(LayoutableContainer& layoutable) {
  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = rect.getWidth() - margins.left - margins.right;
  double height = rect.getHeight() - margins.top - margins.bottom;

  double unit_width;
  double unit_height;

  int sum = std::accumulate(distribution.begin(), distribution.end(), 0);
  sum = std::max(1, sum);

  double local_spacing = 0;
  if (kHorizontal == orientation) {
    unit_width = width / (sum + (sum - 1) * spacing);
    unit_height = height;
    local_spacing = unit_width * spacing;
  } else {
    unit_width = width;
    unit_height = height / (sum + (sum - 1) * spacing);
    local_spacing = unit_height * spacing;
  }

  double x = margins.left;
  double y = margins.top;

  for (size_t index = 0;
      (index < distribution.size()) && (index < layoutable.getNbViews());
      ++index) {
    int dist = distribution.at(index);
    double view_width;
    double view_height;
    if (kHorizontal == orientation) {
      view_width = unit_width * dist + local_spacing * (dist - 1);
      view_height = unit_height;
    } else {
      view_width = unit_width;
      view_height = unit_height * dist + local_spacing * (dist - 1);
    }

    VSTGUI::CRect view_rect(x, y, x + view_width, y + view_height);

    if (kHorizontal == orientation) {
      x += view_width + local_spacing;
    } else {
      y += view_height + local_spacing;
    }

    VSTGUI::CView *view = layoutable.getView(index);
    if (nullptr != view) {
      view->setViewSize(view_rect);
      view->setMouseableArea(view_rect);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
