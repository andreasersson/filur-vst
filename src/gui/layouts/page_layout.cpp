/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/page_layout.h>

#include <filurvst/gui/layoutable_container.h>

#include <algorithm>
#include <cassert>

namespace filurvst {
namespace gui {

static void set_view_id(enum PageLayout::LayoutId id, VSTGUI::CView* view) {
  assert(nullptr != view);

  int32_t attribute_data = id;
  view->setAttribute(Layout::kLayoutAttributeId, sizeof(attribute_data), &attribute_data);
}

static bool get_view_id(VSTGUI::CView* view, int32_t& id) {
  bool ret_val = true;
  uint32_t attribute_size = 0;
  int32_t local_id = 0;

  if (nullptr == view) {
    ret_val = false;
  }

  if (ret_val) {
    ret_val = view->getAttribute(Layout::kLayoutAttributeId,
                                 sizeof(local_id),
                                 &local_id,
                                 attribute_size);
  }

  if (ret_val) {
    ret_val = (sizeof(local_id) == attribute_size);
  }

  if (ret_val) {
    id = local_id;
  }

  return ret_val;
}

void PageLayout::layout(LayoutableContainer& layoutable) {
  assert(header_height >= 0.0);
  assert(header_height <= 1.0);
  assert(spacing >= 0.0);
  assert(spacing <= 1.0);

  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = std::max(rect.getWidth() - margins.left - margins.right, 0.0);
  double height = std::max(rect.getHeight() - margins.top - margins.bottom, 0.0);

  VSTGUI::CView *header = nullptr;
  VSTGUI::CView *content = nullptr;

  for (uint32_t view_index = 0; view_index < layoutable.getNbViews();
      ++view_index) {
    VSTGUI::CView* view = layoutable.getView(view_index);
    int32_t id;
    if (get_view_id(view, id)) {
      switch (id) {
        case kHeaderId:
          header = view;
          break;

        case kContentId:
          content = view;
          break;

        default:
          break;
      }
    }
  }

  double local_header_height = height * header_height;
  assert(local_header_height <= height);
  double local_spacing = height * spacing;
  if (local_header_height + local_spacing > height) {
    local_spacing = height - local_header_height;
  }
  double content_height = height - local_header_height - local_spacing;
  assert(content_height >= 0);
  assert(content_height <= height);

  double x = margins.left;
  double y = margins.top;
  VSTGUI::CRect header_rect(x, y, x + width, y + local_header_height);
  VSTGUI::CRect content_rect(x, height - content_height, x + width, height);

  if (nullptr != header) {
    header->setViewSize(header_rect);
    header->setMouseableArea(header_rect);
  }
  if (nullptr != content) {
    content->setViewSize(content_rect);
    content->setMouseableArea(content_rect);
  }
}

void PageLayout::setHeaderId(VSTGUI::CView* view) {
  set_view_id(kHeaderId, view);
}

void PageLayout::setContentId(VSTGUI::CView* view) {
  set_view_id(kContentId, view);
}

}  // namespace gui
}  // namespace filurvst
