/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/list_layout.h>

#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

void ListLayout::layout(LayoutableContainer& layoutable) {
  int32_t num_views = layoutable.getNbViews();
  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = rect.getWidth() - margins.left - margins.right;
  double height = rect.getHeight() - margins.top - margins.bottom;

  double view_width;
  double view_height;
  double local_spacing;

  if (kHorizontal == orientation) {
    view_width = width / (num_views + (num_views - 1) * spacing);
    view_height = height;
    local_spacing = view_width * spacing;
  } else {
    view_width = width;
    view_height = height / (num_views + (num_views - 1) * spacing);
    local_spacing = view_height * spacing;
  }

  double x = margins.left;
  double y = margins.top;

  for (int child_index = 0; child_index < num_views; ++child_index) {
    VSTGUI::CRect view_rect(x, y, x + view_width, y + view_height);
    if (kHorizontal == orientation) {
      x += view_width + local_spacing;
    } else {
      y += view_height + local_spacing;
    }

    VSTGUI::CView *view = layoutable.getView(child_index);
    if (nullptr != view) {
      view->setViewSize(view_rect);
      view->setMouseableArea(view_rect);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
