/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/basic_layout.h>

#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

void BasicLayout::layout(LayoutableContainer& layoutable) {
  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = rect.getWidth() - margins.left - margins.right;
  double height = rect.getHeight() - margins.top - margins.bottom;
  double x = margins.left;
  double y = margins.top;
  VSTGUI::CRect view_rect(x, y, x + width, y + height);

  for (size_t view_index = 0; view_index < layoutable.getNbViews();
      ++view_index) {
    VSTGUI::CView* view = layoutable.getView(view_index);
    if (nullptr != view) {
      view->setViewSize(view_rect);
      view->setMouseableArea(view_rect);
    }
  }
}

}  // namespace gui
}  // namespace filurvst
