/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/fill_layout.h>

#include <filurvst/gui/layoutable_container.h>

namespace filurvst {
namespace gui {

static bool ignore_margins(const VSTGUI::CView* view) {
  bool ret_val = true;
  uint32_t attribute_size = 0;
  int32_t value = 0;

  if (nullptr == view) {
    ret_val = false;
  }

  if (ret_val) {
    ret_val = view->getAttribute(Layout::kLayoutAttributeId,
                                 sizeof(value),
                                 &value,
                                 attribute_size);
  }

  if (ret_val) {
    ret_val = (sizeof(value) == attribute_size);
  }

  if (ret_val) {
    ret_val = value;
  }

  return ret_val;
}

void FillLayout::layout(LayoutableContainer& layoutable) {
  VSTGUI::CRect rect = layoutable.getViewSize();
  VSTGUI::CRect rect_with_margins = rect;
  const VSTGUI::CRect &margins = layoutable.getMargins();
  rect_with_margins.left += margins.left;
  rect_with_margins.right -= margins.right;
  rect_with_margins.top += margins.top;
  rect_with_margins.bottom -= margins.bottom;

  for (size_t view_index = 0; view_index < layoutable.getNbViews(); ++view_index) {
    VSTGUI::CView* view = layoutable.getView(view_index);
    if (nullptr != view) {
      if (ignore_margins(view)) {
        view->setViewSize(rect);
        view->setMouseableArea(rect);
      } else {
        view->setViewSize(rect_with_margins);
        view->setMouseableArea(rect_with_margins);
      }
    }
  }
}

void FillLayout::setIgnoreMargins(VSTGUI::CView* view, bool value) {
  if (nullptr != view) {
    int32_t attribute_data = value;
    view->setAttribute(Layout::kLayoutAttributeId, sizeof(attribute_data), &attribute_data);
  }
}

}  // namespace gui
}  // namespace filurvst
