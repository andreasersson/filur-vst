/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layouts/parameter_layout.h>

#include <filurvst/gui/layoutable_container.h>

#include <vstgui/lib/controls/ccontrol.h>

#include <algorithm>

namespace filurvst {
namespace gui {

static void set_view_id(enum ParameterLayout::LayoutId id, VSTGUI::CView* view) {
  assert(nullptr != view);

  int32_t attribute_data = id;
  view->setAttribute(Layout::kLayoutAttributeId, sizeof(attribute_data), &attribute_data);
}

static bool get_view_id(VSTGUI::CView* view, int32_t& id) {
  bool ret_val = true;
  uint32_t attribute_size = 0;
  int32_t local_id = 0;

  if (nullptr == view) {
    ret_val = false;
  }

  if (ret_val) {
    ret_val = view->getAttribute(Layout::kLayoutAttributeId,
                                 sizeof(local_id),
                                 &local_id, attribute_size);
  }

  if (ret_val) {
    ret_val = (sizeof(local_id) == attribute_size);
  }

  if (ret_val) {
    id = local_id;
  }

  return ret_val;
}

void ParameterLayout::layout(LayoutableContainer& layoutable) {
  VSTGUI::CRect rect = layoutable.getViewSize();
  const VSTGUI::CRect &margins = layoutable.getMargins();

  double width = std::max(0.0, rect.getWidth() - margins.left - margins.right);
  double height = std::max(0.0, rect.getHeight() - margins.top - margins.bottom);

  VSTGUI::CView *control = nullptr;
  VSTGUI::CView *label = nullptr;

  for (uint32_t index = 0;
      (index < layoutable.getNbViews()) && ((nullptr == label) || (nullptr == control));
      ++index) {
    VSTGUI::CView* view = layoutable.getView(index);
    int32_t id = kControlId;
    get_view_id(view, id);
    if (kLabelId == id) {
      label = view;
    } else {
      control = view;
    }
  }

  double label_height = 0.0;
  if (nullptr != label_font) {
    label_height = 1.25 * label_font->getSize();
  }

  double local_spacing = std::max(0.5, spacing * height);
  double control_height = std::max(0.0, (height - label_height - local_spacing));
  double control_scaled_height = control_scale * (control_height);
  double control_offset = std::max(0.0, (height - label_height - control_scaled_height - local_spacing) * 0.5);

  double x = margins.left;
  double y = margins.top + control_offset;

  VSTGUI::CRect control_rect(x, y, x + width, y + control_scaled_height);

  if (nullptr != control) {
    control->setViewSize(control_rect);
    control->setMouseableArea(control_rect);
  }

  y = margins.top + control_height + local_spacing;
  VSTGUI::CRect label_rect(x, y, x + width, y + label_height);

  if (nullptr != label) {
    label->setViewSize(label_rect);
    label->setMouseableArea(label_rect);
  }
}

void ParameterLayout::setLabelId(VSTGUI::CView* view) {
  set_view_id(kLabelId, view);
}

void ParameterLayout::setControlId(VSTGUI::CView* view) {
  set_view_id(kControlId, view);
}

}  // namespace gui
}  // namespace filurvst
