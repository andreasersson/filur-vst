/*
 * Copyright 2018-2020 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/aboutbox.h>
#include <filurvst/gui/drawable.h>
#include <filurvst/gui/drawable_factory.h>
#include <filurvst/gui/layout.h>
#include <filurvst/gui/layout_factory.h>
#include <filurvst/gui/layoutable.h>
#include <filurvst/gui/layoutable_container.h>
#include <filurvst/gui/uidesc.h>

#include <filurvst/gui/layoutables/layoutable_knob.h>
#include <filurvst/gui/layoutables/layoutable_on_off_button.h>
#include <filurvst/gui/layoutables/layoutable_view_switch_button.h>
#include <filurvst/gui/layoutables/layoutable_option_menu.h>
#include <filurvst/gui/layoutables/layoutable_text.h>
#include <filurvst/gui/layoutables/layoutable_textedit.h>
#include <filurvst/gui/layouts/fill_layout.h>
#include <filurvst/gui/layouts/page_layout.h>
#include <filurvst/gui/layouts/parameter_layout.h>

#include <vstgui/uidescription/detail/uiviewcreatorattributes.h>
#include <vstgui/uidescription/uiviewfactory.h>
#include <vstgui/uidescription/uiviewswitchcontainer.h>
#include <vstgui/vstgui.h>

#include <cassert>
#include <list>
#include <map>
#include <memory>
#include <string>

namespace filurvst {
namespace gui {

static const VSTGUI::IdStringPtr kLayoutable = "Layoutable";
static const VSTGUI::IdStringPtr kLayoutableKnob = "LayoutableKnob";
static const VSTGUI::IdStringPtr kLayoutableSlider = "LayoutableSlider";
static const VSTGUI::IdStringPtr kLayoutableOnOffButton ="LayoutableOnOffButton";
static const VSTGUI::IdStringPtr kViewSwitchButton = "ViewSwitchButton";
static const VSTGUI::IdStringPtr kLayoutableOptionMenu = "LayoutableOptionMenu";
static const VSTGUI::IdStringPtr kLayoutableText = "LayoutableText";
static const VSTGUI::IdStringPtr kLayoutableTextEdit = "LayoutableTextEdit";
static const VSTGUI::IdStringPtr kViewSwitchContainer = "ViewSwitchContainer";
static const VSTGUI::IdStringPtr kPlaceholder = "Placeholder";
static const VSTGUI::IdStringPtr kLayoutableContainer = "LayoutableContainer";
static const VSTGUI::IdStringPtr kAboutBox = "AboutBox";

// Attributes
static const std::string kAttrRadius = "radius";
static const std::string kAttrAspectRatio = "aspect-ratio";
static const std::string kAttrTextEditRadius = "text-edit-radius";
static const std::string kAttrTextEditAspectRatio = "text-edit-aspect-ratio";
static const std::string kAttrOptionMenuRadius = "option-menu-radius";
static const std::string kAttrOptionMenuAspectRatio = "option-menu-aspect-ratio";
static const std::string kAttrSwitchButtonRadius = "switch-button-radius";
static const std::string kAttrSwitchButtonAspectRatio = "switch-button-aspect-ratio";
static const std::string kAttrFont = "font";
static const std::string kAttrTextEditFont = "text-edit-font";
static const std::string kAttrOptionMenuFont = "option-menu-font";
static const std::string kAttrSwitchButtonFont = "switch-button-font";
static const std::string kAttrSize = "size";
static const std::string kAttrMargin = "margin";
static const std::string kAttrOrientation = "orientation";
static const std::string kAttrSymmetric = "symmetric";

static const std::string kAttrValue = "value";
static const std::string kAttrMinValue = "min-value";
static const std::string kAttrMaxValue = "max-value";

static const std::string kAttrDrawable = "drawable";
static const std::string kAttrDrawableText = "text";
static const std::string kAttrDrawableTextRect = "text-rect";
static const std::string kAttrDrawableKnob = "knob";
static const std::string kAttrDrawableSlider = "slider";
static const std::string kAttrDrawableOnOffButton = "on-off-button";

static const std::string kAttrBackgroundColor = "background-color";

static const std::string kAttrInstance = "instance";
static const std::string kAttrInstanceOffset = "instance-offset";

static const std::string kAttrLayoutId = "layout-id";

#if 0 // Not currently used
static inline void left_trim(std::string &string)
{
  string.erase(string.begin(), std::find_if(string.begin(), string.end(), [](int ch) {
            return !std::isspace(ch);
          }));
}

static inline void right_trim(std::string &string)
{
  string.erase(std::find_if(string.rbegin(), string.rend(), [](int ch) {
            return !std::isspace(ch);
          }).base(), string.end());
}

static inline void trim(std::string &string)
{
  left_trim(string);
  right_trim(string);
}
#endif

static bool get_tag(const VSTGUI::UIAttributes& attributes,
                    const VSTGUI::IUIDescription* description,
                    int32_t& tag);
static bool get_attribute(VSTGUI::CView* view,
                          VSTGUI::CViewAttributeID attribute_id,
                          int32_t& value);
static bool has_attribute(VSTGUI::CView* view,
                          VSTGUI::CViewAttributeID attribute_id);
static bool set_attribute(VSTGUI::CView* view,
                          VSTGUI::CViewAttributeID attribute_id,
                          int32_t value);

static void find_controls_with_tag(VSTGUI::CViewContainer* parent,
                                   int32_t tag,
                                   std::vector<VSTGUI::SharedPointer<VSTGUI::CControl>>& controls);

void handle_layout_id(const VSTGUI::UIAttributes& attributes, VSTGUI::CView* view) {
  assert(nullptr != view);

  static const std::map<std::string, int> layout_id_map = {
      { "label", ParameterLayout::kLabelId },
      { "control", ParameterLayout::kControlId },
      { "header", PageLayout::kHeaderId },
      { "content", PageLayout::kContentId } };

  std::string value;
  get_string_attribute(attributes, kAttrLayoutId, value);
  if (layout_id_map.count(value) > 0) {
    int32_t id = layout_id_map.at(value);
    view->setAttribute(Layout::kLayoutAttributeId, sizeof(id), &id);
  }
}

class LayoutableCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  enum AttributeIds {
    kTagId = 10000,
    kTagOffset
  };

  LayoutableCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutable;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return VSTGUI::UIViewCreator::kCControl;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    std::unique_ptr<Drawable> drawable = drawable_factory(attributes, description);
    std::unique_ptr<Layoutable> layoutable(new Layoutable(nullptr, -1, std::move(drawable)));

    if (layoutable) {
      handle_layout_id(attributes, layoutable.get());
      handleInstance(attributes, description, layoutable.get());
    }

    return layoutable.release();
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;

    if (nullptr != view) {
      handle_layout_id(attributes, view);
    }

    Layoutable* layoutable = dynamic_cast<Layoutable*>(view);
    if (nullptr != layoutable) {
      drawable_apply(attributes, description, layoutable->getDrawable());
      ret_val = true;
    }

    VSTGUI::CControl* control = dynamic_cast<VSTGUI::CControl*>(view);
    if (nullptr != control) {
      handleInstance(attributes, description, control);
      ret_val = true;
    }

    return ret_val;
  }

  bool getAttributeNames(std::list<std::string>& attribute_names) const override {
    attribute_names.emplace_back(kAttrInstance);
    attribute_names.emplace_back(kAttrInstanceOffset);

    return true;
  }

  AttrType getAttributeType(const std::string& attribute_name) const override {
    AttrType attribute_type = kUnknownType;

    if (kAttrInstance == attribute_name) {
      attribute_type = kIntegerType;
    } else if (kAttrInstanceOffset == attribute_name) {
      attribute_type = kIntegerType;
    }

    return attribute_type;
  }

 private:
  void handleInstance(const VSTGUI::UIAttributes& attributes,
                      const VSTGUI::IUIDescription* description,
                      VSTGUI::CControl* control) const {
    assert(nullptr != control);

    if (!has_attribute(control, kTagId)) {
      int32_t tag = 0;
      if (get_tag(attributes, description, tag)) {
        set_attribute(control, kTagId, tag);
      }
    }

    if (!has_attribute(control, kTagOffset)) {
      int32_t tag_offset = 0;
      if (attributes.getIntegerAttribute(kAttrInstanceOffset, tag_offset)) {
        set_attribute(control, kTagOffset, tag_offset);
      }
    }

    int32_t instance;
    if (attributes.getIntegerAttribute(kAttrInstance, instance)) {
      int32_t tag;
      int32_t tag_offset;
      if (get_attribute(control, kTagId, tag)
          && get_attribute(control, kTagOffset, tag_offset)) {
        int base_id = instance * tag_offset;
        control->setTag(tag + base_id);
      }
    }
  }
};

class LayoutableKnobCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableKnobCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableKnob;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable Knob";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableKnob);
    }
    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);

    return new LayoutableKnob(nullptr, -1, std::move(drawable));
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;
    LayoutableKnob* knob = dynamic_cast<LayoutableKnob*>(view);

    if (nullptr != knob) {
      drawable_apply(attributes, description, knob->getDrawable());
      ret_val = true;
    }

    return ret_val;
  }

  bool getAttributeNames(std::list<std::string>& attribute_names) const override {
    attribute_names.emplace_back(kAttrDrawable);
    attribute_names.emplace_back(kAttrSymmetric);

    return true;
  }

  AttrType getAttributeType(const std::string& attribute_name) const override {
    AttrType attribute_type = kUnknownType;

    if (kAttrDrawable == attribute_name) {
      attribute_type = kStringType;
    } else if (kAttrSymmetric == attribute_name) {
      attribute_type = kBooleanType;
    }

    return attribute_type;
  }
};

class LayoutableSliderCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableSliderCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableSlider;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable Slider";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableSlider);
    }
    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);
    std::unique_ptr<LayoutableKnob> slider(new LayoutableKnob(nullptr, -1, std::move(drawable)));
    slider->forceLinearMode(true);

    return slider.release();
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;
    LayoutableKnob* slider = dynamic_cast<LayoutableKnob*>(view);

    if (nullptr != slider) {
      drawable_apply(attributes, description, slider->getDrawable());
      ret_val = true;
    }

    return ret_val;
  }

  bool getAttributeNames(std::list<std::string>& attribute_names) const override {
    attribute_names.emplace_back(kAttrOrientation);

    return true;
  }

  AttrType getAttributeType(const std::string& attribute_name) const override {
    AttrType attribute_type = kUnknownType;

    if (kAttrOrientation == attribute_name) {
      attribute_type = kListType;
    }

    return attribute_type;
  }
};

class LayoutableOnOffButtonCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableOnOffButtonCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableOnOffButton;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable On Off Button";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableOnOffButton);
    }
    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);

    return new LayoutableOnOffButton(nullptr, -1, std::move(drawable));
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;
    LayoutableOnOffButton* button = dynamic_cast<LayoutableOnOffButton*>(view);
    if (nullptr != button) {
      drawable_apply(attributes, description, button->getDrawable());
      ret_val = true;
    }

    return ret_val;
  }
};

class ViewSwitchButtonCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  ViewSwitchButtonCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kViewSwitchButton;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable View Switch Button";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    int value = 0;
    double min_value = 0.0;
    double max_value = 1.0;

    description->getVariable("view-switch-button-min", min_value);
    description->getVariable("view-switch-button-max", max_value);
    attributes.getDoubleAttribute(kAttrMinValue, min_value);
    attributes.getDoubleAttribute(kAttrMaxValue, max_value);
    attributes.getIntegerAttribute(kAttrValue, value);

    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableTextRect);
    }

    if (nullptr == local_attributes.getAttributeValue(kAttrRadius)) {
      double radius;
      if (description->getVariable(kAttrSwitchButtonRadius.c_str(), radius)) {
        local_attributes.setDoubleAttribute(kAttrRadius, radius);
      }
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrAspectRatio)) {
      double aspect_ratio;
      if (description->getVariable(kAttrSwitchButtonAspectRatio.c_str(), aspect_ratio)) {
        local_attributes.setDoubleAttribute(kAttrAspectRatio, aspect_ratio);
      }
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrFont)) {
      std::string font;
      if (description->getVariable(kAttrSwitchButtonFont.c_str(), font)) {
        local_attributes.setAttribute(kAttrFont, font);
      }
    }

    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);

    return new LayoutableViewSwitchButton(nullptr, -1, value, min_value, max_value, std::move(drawable));
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;

    LayoutableViewSwitchButton* button = dynamic_cast<LayoutableViewSwitchButton*>(view);

    if (nullptr != button) {
      int value = 0;
      double min_value = 0.0;
      double max_value = 1.0;

      if (attributes.getDoubleAttribute(kAttrMinValue, min_value)) {
        button->setMin(min_value);
      }
      if (attributes.getDoubleAttribute(kAttrMaxValue, max_value)) {
        button->setMax(max_value);
      }
      if (attributes.getIntegerAttribute(kAttrValue, value)) {
        button->setButtonValue(value);
      }

      int32_t instance;
      if (attributes.getIntegerAttribute(kAttrInstance, instance)) {
        button->setButtonValue(instance);
      }

      drawable_apply(attributes, description, button->getDrawable());

      ret_val = true;
    }

    return ret_val;
  }
};

class LayoutableTextCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableTextCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableText;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable Text";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableText);
    }
    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);

    return new LayoutableText(nullptr, -1, std::move(drawable));
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;
    LayoutableText* layoutable = dynamic_cast<LayoutableText*>(view);
    if (nullptr != layoutable) {
      drawable_apply(attributes, description, layoutable->getDrawable());
      ret_val = true;
    }

    return ret_val;
  }
};

class LayoutableOptionMenuCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableOptionMenuCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableOptionMenu;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable Option Menu";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableTextRect);
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrRadius)) {
      double radius;
      if (description->getVariable(kAttrOptionMenuRadius.c_str(), radius)) {
        local_attributes.setDoubleAttribute(kAttrRadius, radius);
      }
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrAspectRatio)) {
      double aspect_ratio;
      if (description->getVariable(kAttrOptionMenuAspectRatio.c_str(), aspect_ratio)) {
        local_attributes.setDoubleAttribute(kAttrAspectRatio, aspect_ratio);
      }
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrFont)) {
      std::string font;
      if (description->getVariable(kAttrOptionMenuFont.c_str(), font)) {
        local_attributes.setAttribute(kAttrFont, font);
      }
    }
    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);

    return new LayoutableOptionMenu(nullptr, -1, std::move(drawable));
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;
    LayoutableOptionMenu* option_menu =
        dynamic_cast<LayoutableOptionMenu*>(view);
    if (nullptr != option_menu) {
      drawable_apply(attributes, description, option_menu->getDrawable());
      ret_val = true;
    }

    return ret_val;
  }
};

class LayoutableTextEditCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableTextEditCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableTextEdit;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutable;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable Text Edit";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIAttributes local_attributes = attributes;
    if (nullptr == local_attributes.getAttributeValue(kAttrDrawable)) {
      local_attributes.setAttribute(kAttrDrawable, kAttrDrawableTextRect);
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrRadius)) {
      double radius;
      if (description->getVariable(kAttrTextEditRadius.c_str(), radius)) {
        local_attributes.setDoubleAttribute(kAttrRadius, radius);
      }
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrAspectRatio)) {
      double aspect_ratio;
      if (description->getVariable(kAttrTextEditAspectRatio.c_str(), aspect_ratio)) {
        local_attributes.setDoubleAttribute(kAttrAspectRatio, aspect_ratio);
      }
    }
    if (nullptr == local_attributes.getAttributeValue(kAttrFont)) {
      std::string font;
      if (description->getVariable(kAttrTextEditFont.c_str(), font)) {
        local_attributes.setAttribute(kAttrFont, font);
      }
    }
    std::unique_ptr<Drawable> drawable = drawable_factory(local_attributes, description);
    std::unique_ptr<LayoutableTextEdit> text_edit(new LayoutableTextEdit(nullptr, -1, std::move(drawable)));

    if (text_edit) {
      std::string font_name;
      if (description->getVariable(kAttrTextEditFont.c_str(), font_name)) {
        VSTGUI::CFontRef font = nullptr;
        get_font(description, &font_name, font);
        if (nullptr != font) {
          text_edit->setFont(font);
        }
      }
    }

    return text_edit.release();
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    assert(nullptr != view);

    bool ret_val = false;
    LayoutableTextEdit* text_edit = dynamic_cast<LayoutableTextEdit*>(view);
    if (nullptr != text_edit) {
      drawable_apply(attributes, description, text_edit->getDrawable());
      ret_val = true;
    }

    return ret_val;
  }
};

class ViewSwitchController : public VSTGUI::UIDescriptionViewSwitchController {
 public:
  ViewSwitchController(VSTGUI::UIViewSwitchContainer* view_switch,
                       const VSTGUI::IUIDescription* description,
                       VSTGUI::IController* controller)
      : VSTGUI::UIDescriptionViewSwitchController(view_switch, description, controller) {}
  virtual ~ViewSwitchController() {
  }

  virtual void switchContainerAttached() override {
    find_controls_with_tag(viewSwitch->getFrame(), switchControlTag, m_controls);
    for (auto& control : m_controls) {
      control->registerControlListener(this);
    }

    if (m_controls.size() > 0) {
      valueChanged(m_controls.at(0));
    }
  }

  virtual void switchContainerRemoved() override {
    for (auto& control : m_controls) {
      control->unregisterControlListener(this);
    }
    m_controls.clear();
    currentIndex = -1;
  }

 private:
  std::vector<VSTGUI::SharedPointer<VSTGUI::CControl>> m_controls;
};

class ViewSwitchContainerCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  ViewSwitchContainerCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kViewSwitchContainer;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return VSTGUI::UIViewCreator::kUIViewSwitchContainer;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "View Switch Container";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& /*attributes*/,
                        const VSTGUI::IUIDescription* description) const override {
    VSTGUI::UIViewSwitchContainer* container = new VSTGUI::UIViewSwitchContainer(VSTGUI::CRect(0, 0, 0, 0));
    //NOTE: The ViewSwitchController will be owned by the UIViewSwitchContainer.
    new ViewSwitchController(container, description, description->getController());

    return container;
  }
};

class PlaceholderCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  PlaceholderCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kPlaceholder;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return nullptr;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Placeholder";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& /*attributes*/,
                        const VSTGUI::IUIDescription* /*description*/) const override {
    return new VSTGUI::CView(VSTGUI::CRect(0, 0, 0, 0));
  }
};

class LayoutableContainerCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  LayoutableContainerCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kLayoutableContainer;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return nullptr;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "Layoutable Container";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    std::unique_ptr<Layout> layout = gui::layout_factory(attributes, description);
    LayoutableContainer* container = new LayoutableContainer(std::move(layout));
    if (nullptr != container) {
      VSTGUI::CRect margin;
      if (attributes.getRectAttribute(kAttrMargin, margin)) {
        container->setMargins(margin);
      }

      VSTGUI::CPoint size;
      if (attributes.getPointAttribute(kAttrSize, size)) {
        VSTGUI::CRect rect = container->getViewSize();
        rect.setSize(size);
        container->setViewSize(rect, true);
        container->setMouseableArea(rect);
      }

      VSTGUI::CColor bg_color;
      if (string_to_color(description,
                          attributes.getAttributeValue(kAttrBackgroundColor),
                          bg_color)) {
        container->setTransparency(false);
        container->setBackgroundColor(bg_color);
      } else {
        container->setTransparency(true);
      }

      handle_layout_id(attributes, container);

      container->setAutosizeFlags(VSTGUI::kAutosizeAll);
    }

    return container;
  }

  bool apply(VSTGUI::CView* view,
             const VSTGUI::UIAttributes& attributes,
             const VSTGUI::IUIDescription* description) const override {
    bool ret_val = false;

    LayoutableContainer* container = dynamic_cast<LayoutableContainer*>(view);
    if (nullptr != container) {
      container->setAutosizeFlags(VSTGUI::kAutosizeAll);
      gui::layout_apply(attributes, description, container->getLayout());

      VSTGUI::CRect margin;
      if (attributes.getRectAttribute(kAttrMargin, margin)) {
        container->setMargins(margin);
      }

      VSTGUI::CPoint size;
      if (attributes.getPointAttribute(kAttrSize, size)) {
        VSTGUI::CRect rect = container->getViewSize();
        rect.setSize(size);
        container->setViewSize(rect, true);
        container->setMouseableArea(rect);
      }

      VSTGUI::CColor bg_color;
      if (string_to_color(description, attributes.getAttributeValue(kAttrBackgroundColor), bg_color)) {
        container->setTransparency(false);
        container->setBackgroundColor(bg_color);
      }

      handle_layout_id(attributes, container);

      int32_t instance;
      int instance_offset = 0;
      VSTGUI::UIAttributes instance_attributes;
      if (attributes.getIntegerAttribute(kAttrInstance, instance)) {
        instance_attributes.setIntegerAttribute(kAttrInstance, instance);
      }
      if (get_attribute(container,
                        LayoutableCreator::kTagOffset,
                        instance_offset)) {
        instance_attributes.setIntegerAttribute(kAttrInstanceOffset, instance_offset);
      } else if (attributes.getIntegerAttribute(kAttrInstanceOffset, instance_offset)) {
        instance_attributes.setIntegerAttribute(kAttrInstanceOffset, instance_offset);
        set_attribute(container, LayoutableCreator::kTagOffset, instance_offset);
      }

      if (instance_attributes.begin() != instance_attributes.end()) {
        for (uint32_t index = 0; index < container->getNbViews(); ++index) {
          description->getViewFactory()->applyAttributeValues(container->getView(index),
                                                              instance_attributes,
                                                              description);
        }
      }

      ret_val = true;
    }

    return ret_val;
  }

  bool getAttributeNames(std::list<std::string>& attribute_names) const override {
    attribute_names.emplace_back(kAttrBackgroundColor);
    attribute_names.emplace_back(kAttrMargin);
    attribute_names.emplace_back(kAttrSize);

    return true;
  }

  AttrType getAttributeType(const std::string& attribute_name) const override {
    AttrType attr_type = kUnknownType;

    if (kAttrBackgroundColor == attribute_name) {
      attr_type = kColorType;
    } else if (kAttrMargin == attribute_name) {
      attr_type = kRectType;
    } else if (kAttrSize == attribute_name) {
      attr_type = kPointType;
    }

    return attr_type;
  }
};

class AboutBoxCreator : public VSTGUI::ViewCreatorAdapter {
 public:
  AboutBoxCreator() {
    VSTGUI::UIViewFactory::registerViewCreator(*this);
  }
  VSTGUI::IdStringPtr getViewName() const override {
    return kAboutBox;
  }
  VSTGUI::IdStringPtr getBaseViewName() const override {
    return kLayoutableContainer;
  }
  VSTGUI::UTF8StringPtr getDisplayName() const override {
    return "AboutBox";
  }
  VSTGUI::CView* create(const VSTGUI::UIAttributes& attributes,
                        const VSTGUI::IUIDescription* description) const override {
    std::unique_ptr<Layout> layout = gui::layout_factory(attributes, description);
    std::unique_ptr<AboutBox> aboutbox(new AboutBox(std::move(layout)));
    filurvst::gui::FillLayout::setIgnoreMargins(aboutbox.get(), true);

    return aboutbox.release();
  }
};

static bool get_tag(const VSTGUI::UIAttributes& attributes,
                    const VSTGUI::IUIDescription* description, int32_t& tag) {
  bool ret_val = true;
  int32_t local_tag = 0;

  const std::string* attribute = attributes.getAttributeValue(VSTGUI::UIViewCreator::kAttrControlTag);
  if (nullptr == attribute) {
    ret_val = false;
  }

  if (ret_val) {
    ret_val = (attribute->length() > 0);
  }

  if (ret_val) {
    local_tag = description->getTagForName(attribute->c_str());
    ret_val = (-1 != local_tag);
  }

  if (ret_val) {
    tag = local_tag;
  }

  return ret_val;
}

static bool get_attribute(VSTGUI::CView* view,
                          VSTGUI::CViewAttributeID attribute_id,
                          int32_t& value) {
  bool ret_val = true;
  uint32_t size = 0;
  int32_t local_value = 0;

  if (nullptr == view) {
    assert(false);
    ret_val = false;
  }

  if (ret_val) {
    ret_val = view->getAttributeSize(attribute_id, size);
  }

  if (ret_val) {
    ret_val = (size == sizeof(value));
  }

  if (ret_val) {
    ret_val = view->getAttribute(attribute_id, sizeof(local_value), &local_value, size);
  }

  if (ret_val) {
    ret_val = (size == sizeof(local_value));
  }

  if (ret_val) {
    value = local_value;
  }

  return ret_val;
}

static bool has_attribute(VSTGUI::CView* view,
                          VSTGUI::CViewAttributeID attribute_id) {
  int32_t value;
  return get_attribute(view, attribute_id, value);
}

static bool set_attribute(VSTGUI::CView* view,
                          VSTGUI::CViewAttributeID attribute_id,
                          int32_t value) {
  bool ret_val = true;

  if (nullptr == view) {
    assert(false);
    ret_val = false;
  }

  if (ret_val) {
    ret_val = view->setAttribute(attribute_id, sizeof(value), &value);
  }

  return ret_val;
}

static void find_controls_with_tag(VSTGUI::CViewContainer* parent,
                                   int32_t tag,
                                   std::vector<VSTGUI::SharedPointer<VSTGUI::CControl>>& controls)
{
  if (nullptr != parent) {
    VSTGUI::ViewIterator it(parent);
    while (*it) {
      VSTGUI::CView* view = *it;
      VSTGUI::CViewContainer* container = view->asViewContainer();
      if (nullptr != container) {
        find_controls_with_tag(container, tag, controls);
      } else {
        auto* control = dynamic_cast<VSTGUI::CControl*>(view);
        if ((nullptr != control) && (control->getTag() == tag)) {
          controls.push_back(control);
        }
      }

      ++it;
    }
  }
}

void ui_view_creator_init() {
  static LayoutableCreator s_layoutable_creator;
  static LayoutableKnobCreator s_layoutable_knobm_creator;
  static LayoutableSliderCreator s_layoutable_slider_creator;
  static LayoutableOnOffButtonCreator s_layoutable_on_off_button_creator;
  static LayoutableOptionMenuCreator s_layoutable_option_menu_creator;
  static LayoutableTextCreator s_layoutable_text_creator;
  static LayoutableTextEditCreator s_layoutable_textedit_creator;
  static ViewSwitchButtonCreator s_layoutable_view_switch_button_creator;
  static ViewSwitchContainerCreator s_view_switch_container_creator;
  static PlaceholderCreator s_placeholder_creator;

  static LayoutableContainerCreator s_layoutable_container_creator;
  static AboutBoxCreator s_about_box_creator;
}

}  // namespace gui
}  // namespace filurvst
