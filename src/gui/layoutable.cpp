/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/gui/layoutable.h>

namespace filurvst {
namespace gui {

Layoutable::Layoutable(VSTGUI::IControlListener* listener,
                       int32_t tag,
                       std::unique_ptr<Drawable> drawable)
    : CControl(VSTGUI::CRect(0, 0, 0, 0), listener, tag), m_drawable(std::move(drawable)) {
  Layoutable::setViewSize(VSTGUI::CRect(0, 0, 0, 0));
}

Layoutable::Layoutable(std::unique_ptr<Drawable> drawable)
    : CControl(VSTGUI::CRect(0, 0, 0, 0), nullptr, -1),m_drawable(std::move(drawable)) {
  Layoutable::setViewSize(VSTGUI::CRect(0, 0, 0, 0));
}

Layoutable::Layoutable(const Layoutable& layoutable)
    : CControl(layoutable) {
}

void Layoutable::setViewSize(const VSTGUI::CRect& rect, bool invalid) {
  VSTGUI::CControl::setViewSize(rect, invalid);
  setMouseableArea(rect);
  resize(rect);

  setDirty();
}

void Layoutable::draw(VSTGUI::CDrawContext* context) {
  if (m_drawable) {
    updateModel(m_model);
    m_drawable->draw(context, getViewSize(), m_model);
  }

  setDirty(false);
}

}  // namespace gui
}  // namespace filurvst
