/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/common/parameter_state.h>

#include <filurvst/common/tlv.h>

#include <cassert>

namespace filurvst {

Steinberg::tresult State::setState(Steinberg::IBStream* stream) {
  Steinberg::IBStreamer streamer(stream, kLittleEndian);

  bool end_of_stream = false;
  while (!end_of_stream) {
    Steinberg::uint32 tag;
    Steinberg::uint32 length;

    bool result = tlv::read(streamer, tag, length);
    bool skip_tlv = false;

    if ((true == result) && (0 == m_parameter_info.count(tag))) {
      skip_tlv = true;
    }

    if ((true == result) && (false == skip_tlv)) {
      if (true == m_parameter_info.at(tag).runtime) {
        skip_tlv = true;
      }
    }

    // NOTE: We currently only support double values.
    if ((true == result) && (length != sizeof(double))) {
      skip_tlv = true;
    }

    if ((true == result) && (true == skip_tlv)) {
      result = tlv::skip(streamer, length);
    }

    if ((true == result) && (false == skip_tlv)) {
      double value = 0.0;
      result = tlv::read_value(streamer, value);
      if (true == result) {
        m_parameters[tag] = value;
      }
    }

    if (false == result) {
      end_of_stream = true;
    }
  }

  return Steinberg::kResultTrue;
}

Steinberg::tresult State::getState(Steinberg::IBStream* stream) {
  Steinberg::IBStreamer streamer(stream, kLittleEndian);

  for (auto &key_value : m_parameter_info) {
    auto &info = key_value.second;

    if (false == info.runtime) {
      auto tag = key_value.first;
      auto parameter_id = key_value.first;
      double value = m_parameters.at(parameter_id);
      tlv::write(streamer, tag, value);
    }
  }

  return Steinberg::kResultTrue;
}

void State::set(Steinberg::Vst::ParamID parameter_id, Steinberg::Vst::ParamValue value) {
  m_parameters[parameter_id] = value;
}

Steinberg::Vst::ParamValue State::get(Steinberg::Vst::ParamID parameter_id) {
  Steinberg::Vst::ParamValue ret_val = 0.0;
  if (m_parameters.count(parameter_id) > 0) {
    ret_val = m_parameters.at(parameter_id);
  }

  return ret_val;
}

void State::setNormalized(Steinberg::Vst::ParamID parameter_id,
                          Steinberg::Vst::ParamValue value) {
  if ((m_parameter_info.count(parameter_id) > 0) &&
      m_parameter_info.at(parameter_id).to_normalized) {
    const parameter_info_t &info = m_parameter_info.at(parameter_id);
    double local_value = info.from_normalized(value, info.min, info.max);
    m_parameters[parameter_id] = local_value;
  }
}

Steinberg::Vst::ParamValue State::getNormalized(Steinberg::Vst::ParamID parameter_id) {
  Steinberg::Vst::ParamValue ret_val = 0.0;
  if (m_parameters.count(parameter_id) > 0) {
    ret_val = m_parameters.at(parameter_id);
  }

  if ((m_parameter_info.count(parameter_id) > 0) &&
      m_parameter_info.at(parameter_id).to_normalized) {
    const parameter_info_t &info = m_parameter_info.at(parameter_id);
    ret_val = info.to_normalized(ret_val, info.min, info.max);
  }

  return ret_val;
}

bool State::info(Steinberg::Vst::ParamID parameter_id, parameter_info_t& info) {
  bool ret_val = false;

  if (m_parameter_info.count(parameter_id) > 0) {
    info = m_parameter_info.at(parameter_id);
    ret_val = true;
  }

  return ret_val;
}

double State::min(Steinberg::Vst::ParamID parameter_id) {
  double min = 0.0;

  if (m_parameter_info.count(parameter_id) > 0) {
    min = m_parameter_info.at(parameter_id).min;
  }

  return min;
}

double State::max(Steinberg::Vst::ParamID parameter_id) {
  double max = 0.0;

  if (m_parameter_info.count(parameter_id) > 0) {
    max = m_parameter_info.at(parameter_id).max;
  }

  return max;

}

void State::add(Steinberg::Vst::ParamID parameter_id,
                double default_value,
                double min,
                double max,
                convert_func from_normalized,
                convert_func to_normalized) {
  m_parameters[parameter_id] = default_value;
  parameter_info_t info;
  info.min = min;
  info.max = max;
  info.from_normalized = from_normalized;
  info.to_normalized = to_normalized;
  info.runtime = false;
  m_parameter_info[parameter_id] = info;
}

void State::addSqr(Steinberg::Vst::ParamID parameter_id,
                   double default_value,
                   double min,
                   double max) {
  add(parameter_id,
      default_value,
      min,
      max,
      normalized_to_value_sqr,
      value_to_normalized_sqr);
}

void State::addInt(Steinberg::Vst::ParamID parameter_id,
                   double default_value,
                   double min,
                   double max) {
  add(parameter_id,
      default_value,
      min,
      max,
      normalized_to_value_int,
      value_to_normalized_int);
}

void State::addUInt(Steinberg::Vst::ParamID parameter_id,
                    double default_value,
                    double min,
                    double max) {
  add(parameter_id,
      default_value,
      min,
      max,
      normalized_to_value_uint,
      value_to_normalized_uint);
}

void State::runtime(Steinberg::Vst::ParamID parameter_id, bool runtime) {
  if (m_parameter_info.count(parameter_id) > 0) {
    m_parameter_info.at(parameter_id).runtime = runtime;
  }
}

void State::remove(Steinberg::Vst::ParamID parameter_id) {
  m_parameters.erase(parameter_id);
  m_parameter_info.erase(parameter_id);
}

}  // namespace filurvst
