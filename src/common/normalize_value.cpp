/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/common/normalize_value.h>

#include <cmath>
#include <algorithm>

namespace filurvst {

double normalized_to_value(double value, double min, double max) {
  return (min + (max - min) * value);
}

double value_to_normalized(double value, double min, double max) {
  double local_value = std::min(std::max(value, min), max);
  return ((local_value - min) / (max - min));
}

double normalized_to_value_sqr(double value, double min, double max) {
  return (min + (max - min) * value * value);
}

double value_to_normalized_sqr(double value, double min, double max) {
  double local_value = std::min(std::max(value, min), max);
  return sqrt((local_value - min) / (max - min));
}

double normalized_to_value_int(double value, double min, double max) {
  return std::round(min + (max - min) * value);
}

double value_to_normalized_int(double value, double min, double max) {
  double local_value = std::min(std::max((double) value, min), max);
  return (static_cast<double>(local_value - min) / (max - min));
}

double normalized_to_value_uint(double value, double min, double max) {
  return std::round(min + max * value);
}

double value_to_normalized_uint(double value, double min, double max) {
  double local_value = std::min(std::max((double) value, min), max);
  return ((local_value - min) / (max - min));
}

} // namespace filurvst
