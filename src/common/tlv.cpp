/*
 * Copyright 2018 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/common/tlv.h>

#include <base/source/fstreamer.h>

#include <cstdint>

namespace filurvst {
namespace tlv {

bool read(Steinberg::IBStreamer& streamer, Steinberg::uint32& tag, Steinberg::uint32& length) {
  bool ret_val = streamer.readInt32u(tag);

  if (true == ret_val) {
    ret_val = streamer.readInt32u(length);
  }

  return ret_val;
}

bool read_value(Steinberg::IBStreamer& streamer, Steinberg::uint32& value) {
  bool ret_val = streamer.readInt32u(value);

  return ret_val;
}

bool read_value(Steinberg::IBStreamer& streamer, Steinberg::int32& value) {
  bool ret_val = streamer.readInt32(value);

  return ret_val;
}

bool read_value(Steinberg::IBStreamer& streamer, double& value) {
  bool ret_val = streamer.readDouble(value);

  return ret_val;
}

bool skip(Steinberg::IBStreamer& streamer, Steinberg::uint32 length) {
  Steinberg::int64 ret_val = streamer.seek(length, Steinberg::kSeekCurrent);

  return (ret_val >= 0);
}

bool write(Steinberg::IBStreamer& streamer, Steinberg::uint32 tag, Steinberg::uint32 value) {
  bool ret_val = streamer.writeInt32u(tag);

  if (true == ret_val) {
    ret_val = streamer.writeInt32u(sizeof(value));
  }

  if (true == ret_val) {
    ret_val = streamer.writeInt32u(value);
  }

  return ret_val;
}

bool write(Steinberg::IBStreamer& streamer, Steinberg::uint32 tag, Steinberg::int32 value) {
  bool ret_val = streamer.writeInt32u(tag);

  if (true == ret_val) {
    ret_val = streamer.writeInt32u(sizeof(value));
  }

  if (true == ret_val) {
    ret_val = streamer.writeInt32(value);
  }

  return ret_val;
}

bool write(Steinberg::IBStreamer& streamer, Steinberg::uint32 tag, double value) {
  bool ret_val = streamer.writeInt32u(tag);

  if (true == ret_val) {
    ret_val = streamer.writeInt32u(sizeof(value));
  }

  if (true == ret_val) {
    ret_val = streamer.writeDouble(value);
  }

  return ret_val;
}

} // namespace tlv
} // namespace filurvst
