/*
 * Copyright 2018-2019 Andreas Ersson
 *
 * This file is part of filurvst.
 *
 * filurvst is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * filurvst is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with filurvst.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <filurvst/common/parameter.h>
#include <filurvst/common/parameter_state.h>

#include <pluginterfaces/base/ustring.h>
#include <pluginterfaces/vst/vsttypes.h>
#include <base/source/fstring.h>
#include <public.sdk/source/vst/vstparameters.h>

#include <memory>

namespace filurvst {

Steinberg::Vst::Parameter* add_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                         State &state,
                                         const std::string& name,
                                         Steinberg::int32 parameter_id,
                                         Steinberg::Vst::UnitID unit_id) {
  return parameters.addParameter(USTRING(name.c_str()),
                                 USTRING(""),
                                 0,
                                 state.getNormalized(parameter_id),
                                 Steinberg::Vst::ParameterInfo::kCanAutomate,
                                 parameter_id,
                                 unit_id);
}

Steinberg::Vst::Parameter* add_range_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                               State &state,
                                               const std::string& name,
                                               Steinberg::int32 parameter_id,
                                               Steinberg::Vst::ParamValue min,
                                               Steinberg::Vst::ParamValue max,
                                               Steinberg::Vst::UnitID unit_id,
                                               int step_count,
                                               to_string_func to_string,
                                               from_string_func from_string) {
  std::unique_ptr<RangeParameter> param(new RangeParameter(USTRING(name.c_str()),
                                                           parameter_id,
                                                           USTRING(""),
                                                           min,
                                                           max,
                                                           state.get(parameter_id),
                                                           step_count));
  param->setToString(to_string);
  param->setFromString(from_string);
  param->setPrecision(1);
  param->setUnitID(unit_id);

  return parameters.addParameter(param.release());
}

Steinberg::Vst::Parameter* add_range_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                               State &state,
                                               const std::string& name,
                                               Steinberg::int32 parameter_id,
                                               Steinberg::Vst::UnitID unit_id) {
  double min = state.min(parameter_id);
  double max = state.max(parameter_id);
  return add_range_parameter(parameters, state, name, parameter_id, min, max, unit_id);
}

Steinberg::Vst::Parameter* add_range_parameter_step(Steinberg::Vst::ParameterContainer &parameters,
                                                    State &state,
                                                    const std::string& name,
                                                    Steinberg::int32 parameter_id,
                                                    Steinberg::Vst::UnitID unit_id) {
  double min = state.min(parameter_id);
  double max = state.max(parameter_id);
  double step_count = max - min;
  return add_range_parameter(parameters, state, name, parameter_id, min, max, unit_id, step_count);
}

Steinberg::Vst::Parameter* add_on_off_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                                State &state,
                                                const std::string& name,
                                                Steinberg::int32 parameter_id,
                                                Steinberg::Vst::UnitID unit_id) {
  return add_range_parameter(parameters, state, name, parameter_id, 0.0, 1.0, unit_id, 1);
}

Steinberg::Vst::Parameter* add_list_parameter(Steinberg::Vst::ParameterContainer &parameters,
                                              State &state,
                                              const std::string& name,
                                              Steinberg::int32 parameter_id,
                                              const std::vector<std::string> &list,
                                              Steinberg::Vst::UnitID unit_id) {
  std::unique_ptr<Steinberg::Vst::StringListParameter> param;
  param.reset(new Steinberg::Vst::StringListParameter(USTRING(name.c_str()), parameter_id));
  param->setUnitID(unit_id);
  param->setNormalized(state.getNormalized(parameter_id));
  for (auto &list_entry : list) {
    param->appendString(Steinberg::String(list_entry.c_str()));
  }

  return parameters.addParameter(param.release());
}

}  // namespace filurvst
