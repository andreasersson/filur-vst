
set(BUILD_TESTS OFF CACHE BOOL "Enable testing.")

if(BUILD_TESTS)
  enable_testing()
  include(${CMAKE_CURRENT_SOURCE_DIR}/cmake/gtest.cmake)
endif(BUILD_TESTS)

function(add_unit_test target src)
  if(BUILD_TESTS)
    set(properties ${ARGV2})

    if(NOT TARGET ${target})
      add_executable(${target} ${src})
      target_link_libraries(${target} PUBLIC gtest_main filurvst base sdk)
      if(APPLE)
        target_link_libraries(${target} PRIVATE "-framework CoreFoundation")
      endif()

      add_test(NAME ${target} COMMAND ${target})
    else()
      target_sources(${target} PRIVATE ${src})
    endif()

    if(properties AND (NOT properties EQUAL ""))
      set_target_properties(${target} PROPERTIES ${properties})
    endif()

  endif(BUILD_TESTS)
endfunction()
